etShader "Custom/NewSurfaceShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            vec3 palette(float d) {
                return mix(vec3(0.2, 0.7, 0.9), vec3(1., 0., 1.), d);
            }

            vec2 rotate(vec2 p, float a) {
                float c = cos(a);
                float s = sin(a);
                return p * mat2(c, s, -s, c);
            }

            float map(vec3 p) {
                for (int i = 0; i < 8; ++i) {
                    float t = iTime * 0.2;
                    p.xz = rotate(p.xz, t);
                    p.xy = rotate(p.xy, t * 1.89);
                    p.xz = abs(p.xz);
                    p.xz -= .5;
                }
                return dot(sign(p), p) / 5.;
            }

            vec4 rm(vec3 ro, vec3 rd) {
                float t = 0.;
                vec3 col = vec3(0.);
                float d;
                for (float i = 0.; i < 64.; i++) {
                    vec3 p = ro + rd * t;
                    d = map(p) * .5;
                    if (d < 0.02) {
                        break;
                    }
                    if (d > 100.) {
                        break;
                    }
                    //col+=vec3(0.6,0.8,0.8)/(400.*(d));
                    col += palette(length(p) * .1) / (400. * (d));
                    t += d;
                }
                return vec4(col, 1. / (d * 100.));
            }
            void mainImage(out vec4 fragColor, in vec2 fragCoord)
            {
                vec2 uv = (fragCoord - (iResolution.xy / 2.)) / iResolution.x;
                vec3 ro = vec3(0., 0., -50.);
                ro.xz = rotate(ro.xz, iTime);
                vec3 cf = normalize(-ro);
                vec3 cs = normalize(cross(cf, vec3(0., 1., 0.)));
                vec3 cu = normalize(cross(cf, cs));

                vec3 uuv = ro + cf * 3. + uv.x * cs + uv.y * cu;

                vec3 rd = normalize(uuv - ro);

                vec4 col = rm(ro, rd);


                fragColor = col;
            }

            /** SHADERDATA
            {
                "title": "fractal pyramid",
                "description": "",
                "model": "car"
            }
            */
        }
        ENDCG
    }
    FallBack "Diffuse"
}
