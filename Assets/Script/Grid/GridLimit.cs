using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class GridLimit : MonoBehaviour
{
    public GridHandler _gridHandler;
    private BoxCollider2D _collider;
    private LevelManager _levelManager;

    private void Start()
    {
        _levelManager = LevelManager.Instance;
        // Pick the parent (should be the grid handler)
    }

    public void ResizeCollider(Vector2 mapSize, Vector2 additionalSquare, float gridSize)
    {
        _collider = GetComponent<BoxCollider2D>();
        _collider.offset = Vector2.zero;
        _collider.size = (mapSize + new Vector2(additionalSquare.x * 2 , additionalSquare.y * 2)) * gridSize * 2;
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent<SpatialMode>(out SpatialMode mode))
        {
            mode.Unsubscribe();
            if (!mode.isDestroyed)
                _levelManager.Error(mode.transform.position, ErrorType.OutOfSimulation);
        }
    }
}
