using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.InputSystem;

// This class should be a child of the main Grid. It will be used to drag it, and to display
// continuously a tiling background.
public class GridBack : MonoBehaviour, IDraggable
{
    public GridHandler _gridHandler;
    private Camera _mainCamera;
    private BoxCollider2D _collider;
    private LevelManager _levelManager;

    private void Awake()
    {
        // Find the camera
        _mainCamera = Camera.main;
        _levelManager = LevelManager.Instance;
        // Pick the parent (should be the grid handler)
        _gridHandler = gameObject.transform.parent.gameObject.GetComponent<GridHandler>();
        // Ensure it is on the good sorting layer (visibility):
        if (!TryGetComponent(out SortingGroup s))
        {
            gameObject.AddComponent(typeof(SortingGroup));
        }
        GetComponent<SortingGroup>().sortingLayerName = "GridBack";
        // Ensure it is on the good layer (for ray tracing):
        gameObject.layer = LayerMask.NameToLayer("GridBack");
        // Ensure it has a large enough 2D collider
        if (!TryGetComponent(out _collider))
        {
            _collider = (BoxCollider2D)gameObject.AddComponent(typeof(BoxCollider2D));
        }
        _collider.offset = new Vector2(0,0);
        transform.Translate(Vector3.forward * 10);
    }

    #region Drag Functions
    // Used during grid dragging (to scroll)
    private Vector3 _initialGridPosition;
    private Vector3 _initialMousePosition;
    // Check: What is this for?
    public bool noDrag { get; set; }

    public void BeginDrag(Vector3 position, Vector2 mousePosition)
    {
        _initialGridPosition = _gridHandler.transform.position;
        _initialMousePosition = _mainCamera.ScreenToWorldPoint(mousePosition);
    }

    public void DragObject(Vector3 position, Vector2 mousePosition)
    {
        // We are scrolling the grid. We have the following invariant:
        // _initialGridPosition - _initialMousePosition = _currentGridPosition - _currentMousePosition
        // therefore:
        Vector3 worldPos = _mainCamera.ScreenToWorldPoint(mousePosition);
        _gridHandler.MoveGrid(worldPos + (_initialGridPosition - _initialMousePosition));
        // To fake infinite grid, we always "center back" the GridBack position
        Vector2Int coordNearCenter = _gridHandler.WorldToGridCoord(Vector3.zero);
        Vector3 newPosition = _gridHandler.GridCoordToWorldNotCentered(coordNearCenter);
        newPosition.z = transform.position.z;
        transform.position = newPosition;
    }

    public void EndDrag(Vector3 position, Vector2 mousePosition)
    {
    }

    #endregion

}
