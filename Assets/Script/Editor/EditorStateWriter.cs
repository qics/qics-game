using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(StateWriter))]
public class EditorStateWrite : Editor
{

    SerializedProperty stateWriter;

    void OnEnable()
    {
        stateWriter = serializedObject.FindProperty("stateWriter");
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var t = (target as StateWriter);
        if (GUILayout.Button("Write State"))
        {
            t.WriteState();
        }

    }
}
