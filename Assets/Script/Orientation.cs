using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Orientation
{
    Up,
    Down,
    Left,
    Right

}

public static class ExtensionOrientation
{
    public static Orientation NextClockwiseOrientation(this Orientation curOrientation)
    {
        Orientation newOrientation = curOrientation switch
        {
            Orientation.Left => (Orientation.Up),
            Orientation.Right => (Orientation.Down),
            Orientation.Down => (Orientation.Left),
            Orientation.Up => (Orientation.Right),
            _ => (Orientation.Right),
        };
        return (newOrientation);
    }
}
