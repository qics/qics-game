﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.InputSystem;

public abstract class Instrument : MonoBehaviour, IDraggable, INeedInputManager, INeedHistoryManager
{
    protected int[] _specificInstruData;
    protected LevelManager _levelManager;
    protected IInputManager _inputManager;
    protected IHistoryManager _historyManager;
    [SerializeField] bool isDevPlaced = false;
    [SerializeField] InstrumentTicket _prefabTicket;

    private GridHandler _gridHandler;

    public IInputManager InputManager
    {
        get
        {
            if (_inputManager == null)
                _levelManager.QueryInputManager(this);
            return (_inputManager);
        }
    }
    public IHistoryManager HistoryManager
    {
        get
        {
            if (_inputManager == null)
                _levelManager.QueryInputManager(this);
            return (_historyManager);
        }
    }

    public bool isOnGrid = false;

    public bool noDrag { get; set; }
    public GameObject selector;
    public Sprite _sprite;
    public SpriteRenderer spriteObject;
    //Should not be public
    public bool isDraggable = true;
    protected Vector3? _initialPosition = null;
    public Orientation m_orientation = Orientation.Left;
    public GameObject selector_prefab;
    public InstrumentTicket associatedTicket;
    public Faketon associatedFaketon;

    public bool isLinked = false;
    public static int instNumber = 0;

    public void Awake()
    {
        _levelManager = LevelManager.Instance;
        _levelManager.QueryHistoryManager(this);
        if (selector == null)
        {
            selector = transform.Find("selector").gameObject;
            if (selector == null)
            {
                Debug.LogError("Unable to find a selector for objet " + gameObject.name);
                Destroy(gameObject);
            }

        }
        if (spriteObject == null)
        {
            spriteObject = transform.Find("Sprite").GetComponent<SpriteRenderer>();
            if (spriteObject == null)
            {
                Debug.LogError("Unable to find a sprite for object " + gameObject.name);
                Destroy(gameObject);
            }
        }
        selector.SetActive(false);
        _initialPosition = null;
        if (isDraggable)  // only draggable instruments are player put.
            instNumber += 1;
        InitInstrument();
    }

    public void Start()
    {
        if (isDevPlaced == true)
        {
            if (_prefabTicket)
            {
                int id = _prefabTicket.GetId();
                _gridHandler = _levelManager.gridHandler;
                associatedTicket = _gridHandler.GetInstrumentTicketFromId(_prefabTicket.GetId());
                associatedFaketon = associatedTicket.associatedFaketon;
            }
            transform.position = _levelManager.gridHandler.NearestGridCenter(transform.position);
            _initialPosition = transform.position;
            isDraggable = false;
            if (_levelManager.gridHandler.LockCell((Vector3)_initialPosition, this) == false)
            {
                Instrument.Destroy(this);
            }
            else
            {
                if (associatedTicket is TicketReceptorMulti)
                    ((TicketReceptorMulti) associatedTicket).RemoveCopie((ReceptorMultiPos) this);
                else
                    associatedTicket.RemoveCopie();
                isOnGrid = true;
            }

        }
    }

    public abstract void InitInstrument();

    public abstract void OnTriggerEnter2D(Collider2D collision);

    #region Drag Functions

    public virtual void BeginDrag(Vector3 position, Vector2 mousePosition)
    {
        _initialPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        spriteObject.sortingLayerName = "FloatingObjects";
        /*Spawner spawn = (Spawner)this;
        if (spawn != null)
        {
            spawn.DragCorrectif();
        }*/
        _levelManager.gridHandler.FreeCell(transform.position);
        transform.Translate(Vector3.forward * -5f);
        noDrag = true;
    }

    public virtual void DragObject(Vector3 position, Vector2 mousePosition)
    {
        if (isDraggable)
        {
            if (noDrag)
            {
                InputManager.ClearSelection();
                noDrag = false;
            }
            transform.position = new Vector3(position.x, position.y, 10f);
        }

    }

    public virtual void EndDrag(Vector3 position, Vector2 mousePosition)
    {
        Vector2Int gridPosition = _levelManager.gridHandler.WorldToGridCoord(position);
        Ray inputRay = Camera.main.ScreenPointToRay(mousePosition);
        RaycastHit2D hitResults = new RaycastHit2D();

        spriteObject.sortingLayerName = "Instruments";
        transform.Translate(Vector3.forward * 5f);
        hitResults = Physics2D.Raycast(inputRay.origin, inputRay.direction, 1000f, LayerMask.GetMask("GameUI"));
        if (!isDraggable || _levelManager.gridHandler.IsLocked(gridPosition) || hitResults.transform != null)
        {
            if (isOnGrid == false || hitResults.transform != null)
                Instrument.Destroy(this);
            else
            {
                transform.position = (Vector3)_initialPosition;
                _levelManager.gridHandler.LockCell((Vector3)_initialPosition, this);
            }

        }
        else
        {
            transform.position = _levelManager.gridHandler.NearestGridCenter(gridPosition);
            Vector2Int initialGrid = _levelManager.gridHandler.WorldToGridCoord((Vector3)_initialPosition);
            if (isOnGrid && initialGrid != gridPosition)
                _historyManager.AddMoveInstrumentAction(gridPosition, initialGrid);
            else if (isOnGrid == false)
                _historyManager.AddSpawnInstrumentAction(gridPosition);
            _levelManager.gridHandler.LockCell(gridPosition, this);
        }
        if (noDrag)
        {
            if (!InputManager.IsSelected(gameObject))
            {
                InputManager.ClearSelection();
                selector.SetActive(true);
                InputManager.AddToSelection(gameObject);
            }
            else
                InputManager.ClearSelection();
        }
    }

    #endregion

    public Faketon GetAssociatedFaketon()
    {
        return (associatedFaketon);
    }

    public virtual Orientation RotateClockwise(bool forceRotation = false)
    {
        if (isDraggable || forceRotation)
        {
            spriteObject.transform.Rotate(Vector3.forward * -90f);

            m_orientation = m_orientation switch
            {
                Orientation.Left => (Orientation.Up),
                Orientation.Right => (Orientation.Down),
                Orientation.Down => (Orientation.Left),
                Orientation.Up => (Orientation.Right),
                _ => (Orientation.Right),
            };
        }
        return (m_orientation);
    }

    public virtual Orientation RotateCounterClockwise(bool forceRotation = false)
    {
        if (isDraggable || forceRotation)
        {
            spriteObject.transform.Rotate(Vector3.forward * 90f);

            m_orientation = m_orientation switch
            {
                Orientation.Left => (Orientation.Down),
                Orientation.Right => (Orientation.Up),
                Orientation.Down => (Orientation.Right),
                Orientation.Up => (Orientation.Left),
                _ => (Orientation.Right),
            };
        }
        return (m_orientation);
    }

    public void GiveOrientation(Orientation newOrientation, bool forceRotation = false)
    {
        while (m_orientation != newOrientation)
            RotateClockwise(forceRotation);
    }

    public static void Destroy(Instrument instrument)
    {
        Connector instrumentConnector;
        
        if (!instrument.associatedTicket.isUnlimited && !instrument.isDevPlaced)
        {
            
            if (instrument.associatedTicket is TicketReceptorMulti)
            {
                TicketReceptorMulti _ticketReceptor = (TicketReceptorMulti)instrument.associatedTicket;
                _ticketReceptor.AddCopie((ReceptorMultiPos)instrument);
            }
            else
                instrument.associatedTicket.AddCopie();
            instrument.associatedTicket.spriteRenderer.color = Color.white;
        }
        


        instrumentConnector = instrument.GetComponentInChildren<Connector>();
        if (instrumentConnector)
        {
            while (instrumentConnector.classicalThreads.Count > 0)
                ClassicalThread.Destroy(instrumentConnector.classicalThreads[0]);

        }
        var clearTrigger = instrument as ITriggerClear;
        clearTrigger?.UnsubscribeClear();
        var cycleTrigger = instrument as ITriggerCycle;
        cycleTrigger?.UnsubscribeCycle();
        Destroy(instrument.gameObject);
    }

    public int[] GetSpecificData()
    {
        return (_specificInstruData);
    }

    public abstract void SetSpecificData(int[] specificData);

    public void SetInputManager(IInputManager manager)
    {
        _inputManager = manager;
    }

    public void SetHistoryManager(IHistoryManager manager)
    {
        _historyManager = manager;
    }
}
