﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(BoxCollider2D))]
public class ReceptorMultiPos : Instrument
{
    private TicketReceptorMulti _receptorManager;

    public TicketReceptorMulti ReceptorManager 
    { 
        get 
        {
            if (_receptorManager == null)
                _receptorManager = (TicketReceptorMulti)associatedTicket;
            return (_receptorManager); 
        } 
        set { _receptorManager = value; } 
    }

    public override void InitInstrument()
    {
        
    }

    /// <summary>
    /// When a new SpatialMode enter, we unsubscribe it and remove it from the collision
    /// we check wether the Faketon owner is the same as the owner of previously entered
    /// mode. We raise an error if it don't fits.
    /// </summary>
    /// <param name="collision"></param>
    public override void OnTriggerEnter2D(Collider2D collision)
    {
        collision.gameObject.TryGetComponent<SpatialMode>(out var newlyEnteredSpatialMode);
        if (newlyEnteredSpatialMode)
        {
            newlyEnteredSpatialMode.Unsubscribe();
            ReceptorManager.AddMode(newlyEnteredSpatialMode);
        }
    }

    public override void SetSpecificData(int[] specificData)
    {
        //throw new System.NotImplementedException();
    }
}
