﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;
using UnityEngine.Serialization;
using TMPro;

public class InstrumentTicket : MonoBehaviour, ITicket
{
    public IDraggable spawnedObject
    {
        get;
        set;
    }

    private Instrument _instrument;

    private LevelManager _levelManager;
    public LevelManager LvlManager
    {
        get
        {
            if (_levelManager == null)
                return (LevelManager.Instance);
            return (_levelManager);
        }
    }

    private Grid _levelGrid;
    [SerializeField] private int _id;
    [SerializeField] TMP_Text _remainingCopiesText;

    public GameObject instrumentGameObject;
    public Faketon associatedFaketon;
    public SpriteRenderer spriteRenderer;
    public bool isUnlimited = true;
    [SerializeField] protected int remainingCopies = 1;

    private int _maxNumberOfCopies;

    private void Awake()
    {
        // Get the levelManager and grid: required to parent correctly the spawned objects
        _levelGrid = LvlManager.levelGrid;
        _instrument = instrumentGameObject.GetComponent<Instrument>();
        if (_instrument == null)
            Destroy(gameObject);

        spawnedObject = instrumentGameObject.GetComponent<IDraggable>();
        if (spawnedObject == null)
            Destroy(gameObject);

        if (spriteRenderer == null)
        {
            Transform spriteObject = transform.Find("Sprite");

            if (spriteObject == null)
            {
                Debug.LogError("No sprite child for " + name);
            }
            else
            {
                if (!spriteObject.TryGetComponent(out spriteRenderer))
                {
                    spriteObject.gameObject.AddComponent(typeof(SpriteRenderer));
                    spriteRenderer = GetComponent<SpriteRenderer>();
                }
                spriteRenderer.sprite = _instrument._sprite;
            }
        }
        if (remainingCopies == 0)
            spriteRenderer.color = new Color(0.4f, 0.4f, 0.4f);
        WriteRemainingCopies();

            
    }

    protected void WriteRemainingCopies()
    {
        if (_remainingCopiesText != null)
        {
            if (isUnlimited)
                _remainingCopiesText.text = "∞";
            else
                _remainingCopiesText.text = remainingCopies.ToString();
        }

    }

    public virtual Transform InstantiateDraggableObject(Vector3 position)
    {
        Transform instantiatedTransform;
        Instrument instantiatedInstrument;

        if (isUnlimited == false && remainingCopies == 0)
            return (null);

        instantiatedTransform = Instantiate(instrumentGameObject, position, Quaternion.identity, LvlManager.gridHandler.transform).transform;
        instantiatedInstrument = instantiatedTransform.GetComponent<Instrument>();
        instantiatedInstrument.associatedTicket = this;
        instantiatedInstrument.associatedFaketon = associatedFaketon;
        //instantiatedInstrument.transform.Translate(Vector3.back);
        //instantiatedInstrument.BroadcastMessage("WriteState", SendMessageOptions.DontRequireReceiver);
        //Debug.Log(name);
        RemoveCopie();
        return (instantiatedTransform);
    }

    public virtual void AddCopie()
    {
        if (!isUnlimited)
        {
            remainingCopies += 1;
            spriteRenderer.color = Color.white;
            WriteRemainingCopies();
        }
    }

    public virtual void RemoveCopie()
    {
        if (!isUnlimited && remainingCopies > 0)
        {
            remainingCopies -= 1;
            if (remainingCopies == 0)
                spriteRenderer.color = new Color(0.4f, 0.4f, 0.4f);
            WriteRemainingCopies();
        }
    }

    public int GetId()
    {
        return (_id);
    }

}
