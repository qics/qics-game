using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

public class PhysicsMeasure : Instrument, ITriggerCycle, IClassicalEmitter
{
    private List<SpatialMode> _inputModes;
    private bool _isTriggered = false;
    private Connector _connector;
    [SerializeField] Slider _numberMeasuredSlider;
    //[SerializeField] Sprite[] _sprites;
    [SerializeField] Faketon[] _faketonsToDraw;
    [SerializeField] FaketonDrawerUI _drawer;
    //[SerializeField] Slider _destructiveSlider;

    public int numberMeasured = 1;
    public bool destructiveMeasurement = false;

    public override void InitInstrument()
    {
        _inputModes = new List<SpatialMode>();
        _specificInstruData = new int[1];
        if (_numberMeasuredSlider == null)
            Debug.LogError("Error: Slider unassigned to object " + name);
        else
        {
            _numberMeasuredSlider.InitSlider(this);
        }
        _connector = GetComponentInChildren<Connector>();
        if (_drawer == null)
            _drawer = GetComponentInChildren<FaketonDrawerUI>();
        SubscribeClear();
    }

    public void TriggerCycle()
    {
        int i = 0;
        while (i < _inputModes.Count)
        {
            if (_inputModes[i].isDestroyed == true)
                _inputModes.RemoveAt(i);
            else
                i = i + 1;
        }
        if (_inputModes.Count == 0)
            return;
        bool result = SpatialMode.MeasureModes(_inputModes, numberMeasured, false);
        SendSignal(result);
        foreach (SpatialMode mode in _inputModes)
        {
            if (mode != null)
            {
                mode.TriggerCycle();
                mode.Subscribe();
            }
        }
        _inputModes.Clear();
        _isTriggered = false;
        UnsubscribeCycle();


    }

    public void SendSignal(bool signal)
    {
        // To only send positive signal from now on
        if (signal)
            _connector.SendSignal(signal);
    }



    public override void SetSpecificData(int[] specificData)
    {
        //_destructiveSlider.ChangeValueBool(specificData[0]);
        _numberMeasuredSlider.ChangeValue(specificData[0]);
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        var newlyEnteredSpatialMode = collision.gameObject.GetComponent<SpatialMode>();
        if (newlyEnteredSpatialMode.IsInCollidingLayer())
        {
            if (!_isTriggered)
            {
                _isTriggered = true;
                SubscribeCycle();
            }
            newlyEnteredSpatialMode.AddInstrument(this);
            newlyEnteredSpatialMode.Unsubscribe();
            _inputModes.Add(newlyEnteredSpatialMode);
        }

    }



    public void OnTriggerExit2D(Collider2D collision)
    {
        SpatialMode mode = collision.GetComponent<SpatialMode>();
        mode.RemoveInstrument(this);
    }


    public void SubscribeCycle()
    {
        _levelManager.StartCycle += TriggerCycle;
    }

    public void UnsubscribeCycle()
    {
        _levelManager.StartCycle -= TriggerCycle;
    }

    /*public void ChangeMeasureMode(int newValue)
    {
        if (newValue == 0)
        {
            destructiveMeasurement = false;
            _specificInstruData[0] = 0;
        }
        else
        {
            _specificInstruData[0] = 1;
            destructiveMeasurement = true;
        }

    }*/

    public void SubscribeClear()
    {
        _levelManager.ClearEvent += TriggerClear;
    }

    public void UnsubscribeClear()
    {
        _levelManager.ClearEvent -= TriggerClear;
    }

    public void TriggerClear()
    {
        _inputModes.Clear();
        _isTriggered = false;
        UnsubscribeCycle();
    }

    public void ChangeNumberMeasured(int newValue)
    {
        numberMeasured = newValue;
        _specificInstruData[0] = newValue;
        _drawer.SetFaketonToDraw(_faketonsToDraw[newValue - 1]);
    }
}
