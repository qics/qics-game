﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class ChangeOfDir : Instrument
{

    public override void InitInstrument()
    {

    }

    public override void SetSpecificData(int[] specificData)
    {
        //throw new System.NotImplementedException();
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        var newlyEnteredSpatialMode = collision.gameObject.GetComponent<SpatialMode>();
        newlyEnteredSpatialMode.orientation = m_orientation;
    }


}
