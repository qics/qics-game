using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TicketReceptorMulti : InstrumentTicket, ITriggerCycle, ITriggerClear
{
    [SerializeField] int numberOfRequiredCopies = 5;
    [SerializeField] FaketonDrawerUI _drawer;
    [SerializeField] LineRenderer _lineRenderer;

    private List<SpatialMode> _enteredModeList;
    private bool _isWon = false;
    private TMP_Text _counterText;
    private int _currentNumberOfCopiesReceived = 0;
    private List<ReceptorMultiPos> _receptorList = null;
    private Camera _mainCamera;
    private float _originalWidthLine = 0.1f;
    private float _originalCamSize = 5f;

    public List<ReceptorMultiPos> ReceptorList
    {
        get 
        {
            if (_receptorList == null)
                _receptorList = new List<ReceptorMultiPos>();
            return (_receptorList);
        }


    }

    internal TMP_Text CounterText
    {
        get
        {
            if (_counterText == null)
                _counterText = GetComponentInChildren<TMP_Text>();
            return (_counterText);
        }
    }
    internal int CurrentNumberOfCopiesReceived
    {
        get { return (_currentNumberOfCopiesReceived); }
        set
        {
            _currentNumberOfCopiesReceived = value;
            CounterText.text = _currentNumberOfCopiesReceived.ToString() + "/" + numberOfRequiredCopies.ToString();
        }
    }

    private void Awake()
    {
        CurrentNumberOfCopiesReceived = _currentNumberOfCopiesReceived; ;
    }

    private void Start()
    {
        _enteredModeList = new List<SpatialMode>();
        _mainCamera = Camera.main;

        SubscribeClear();
    }

    private void Update()
    {
        if (_lineRenderer)
        {
            _lineRenderer.positionCount = ReceptorList.Count;
            _lineRenderer.widthMultiplier = _originalWidthLine * _mainCamera.orthographicSize / _originalCamSize;
            for (int i = 0; i < _lineRenderer.positionCount; i++)
            {
                _lineRenderer.SetPosition(i, new Vector3(ReceptorList[i].transform.position.x, ReceptorList[i].transform.position.y, ReceptorList[i].transform.position.z + 10f));
                //_lineRenderer.SetPosition(i, spatialModes[i].transform.localPosition);
            }
        }
    }

    public bool CleanEnteredMode()
    {
        int i = 0;
        while (i < _enteredModeList.Count)
        {
            if (_enteredModeList[i].isDestroyed == true)
                _enteredModeList.RemoveAt(i);
            else
                i = i + 1;
        }
        if (_enteredModeList.Count == 0)
        {
            UnsubscribeCycle();
            return (false);
        }
        return (true);
    }

    public void RedrawWithPhase()
    {
        _drawer.RedrawWithPhase();
    }

    public override Transform InstantiateDraggableObject(Vector3 position)
    {
        Transform instantiatedTransform;
        ReceptorMultiPos instantiatedReceptor;

        if (isUnlimited == false && remainingCopies == 0)
            return (null);

        instantiatedTransform = Instantiate(instrumentGameObject, position, Quaternion.identity, LvlManager.gridHandler.transform).transform;
        instantiatedReceptor = instantiatedTransform.GetComponent<ReceptorMultiPos>();
        instantiatedReceptor.associatedTicket = this;
        instantiatedReceptor.associatedFaketon = associatedFaketon;
        instantiatedReceptor.ReceptorManager = this;
        //ReceptorList.Add(instantiatedReceptor);
        RemoveCopie(instantiatedReceptor);
        return (instantiatedTransform);
    }

    public void AddMode(SpatialMode newlyEnteredSpatialMode)
    {
        if (newlyEnteredSpatialMode.owner.CurrentReceptor == null || newlyEnteredSpatialMode.owner.CurrentReceptor == this)
        {
            if (_enteredModeList.Count == 0)
            {
                newlyEnteredSpatialMode.owner.CurrentReceptor = this;
                _enteredModeList.Add(newlyEnteredSpatialMode);
                SubscribeCycle();
            }
            else if (newlyEnteredSpatialMode.owner == _enteredModeList[0].owner)
            {
                _enteredModeList.Add(newlyEnteredSpatialMode);
                if (_enteredModeList.Count > associatedFaketon.numberOfSpatialModes)
                    RaiseWrongFaketon();
            }
            else
            {
                RaiseTwoDifferentState();
            }
        }
        else
        {
            if (newlyEnteredSpatialMode.owner.CurrentReceptor.CleanEnteredMode())
            {
                RaiseSeveralOwner();
            }
            else
            {
                if (_enteredModeList.Count == 0)
                {
                    newlyEnteredSpatialMode.owner.CurrentReceptor = this;
                    _enteredModeList.Add(newlyEnteredSpatialMode);
                    SubscribeCycle();
                }
                else if (newlyEnteredSpatialMode.owner == _enteredModeList[0].owner)
                {
                    _enteredModeList.Add(newlyEnteredSpatialMode);
                    if (_enteredModeList.Count > associatedFaketon.numberOfSpatialModes)
                        RaiseWrongFaketon();
                }
                else
                {
                    RaiseTwoDifferentState();
                }
            }
        }

    }
   
    public void AddCopie(ReceptorMultiPos receptor)
    {
        ReceptorList.Remove(receptor);
        if (!isUnlimited)
        {
            remainingCopies += 1;
            spriteRenderer.color = Color.white;
            WriteRemainingCopies();
        }
    }

    public void RemoveCopie(ReceptorMultiPos receptor)
    {
        ReceptorList.Add(receptor);
        if (!isUnlimited && remainingCopies > 0)
        {;
            remainingCopies -= 1;
            if (remainingCopies == 0)
                spriteRenderer.color = new Color(0.4f, 0.4f, 0.4f);
            WriteRemainingCopies();
        }
    }

    public void TriggerCycle()
    {
        if (!CleanEnteredMode())
            return;

        // We check if we have all the modes of this specific faketon and only act when it's the case
        if (_enteredModeList[0].owner.numberOfSpatialModes == _enteredModeList.Count)
        {
            Faketon commonOwner;

            commonOwner = _enteredModeList[0].owner;
            if (Faketon.IsEqual(commonOwner, associatedFaketon))
            {
                if (CurrentNumberOfCopiesReceived < numberOfRequiredCopies)
                {
                    CurrentNumberOfCopiesReceived += 1;
                    if (CurrentNumberOfCopiesReceived == numberOfRequiredCopies / 4)
                        LvlManager.QuarterReceptor();
                    else if (CurrentNumberOfCopiesReceived == numberOfRequiredCopies / 2)
                        LvlManager.HalfReceptor();
                    else if (!_isWon && CurrentNumberOfCopiesReceived == numberOfRequiredCopies)
                    {
                        _isWon = true;
                        LvlManager.WinReceptor();
                    }
                }

            }
            else
            {
                RaiseWrongFaketon();
            }
            Faketon.DestroyTotally(commonOwner);
            _enteredModeList.Clear();
            UnsubscribeCycle();
        }
    }

    private void RaiseWrongFaketon()
    {
        LvlManager.Error(transform.position, ErrorType.WrongFaketon);
    }

    private void RaiseSeveralOwner()
    {
        LvlManager.Error(transform.position, ErrorType.SeveralOwner);
    }

    private void RaiseTwoDifferentState()
    {
        LvlManager.Error(transform.position, ErrorType.TwoDifferentState);
    }

    public void SubscribeCycle()
    {
        LvlManager.StartReceptorCycle += TriggerCycle;
    }

    public void UnsubscribeCycle()
    {
        LvlManager.StartReceptorCycle -= TriggerCycle;
    }

    public void SubscribeClear()
    {
        LvlManager.ClearEvent += TriggerClear;
    }

    public void UnsubscribeClear()
    {
        LvlManager.ClearEvent -= TriggerClear;
    }

    public void TriggerClear()
    {
        UnsubscribeCycle();
        _enteredModeList.Clear();
        _isWon = false;
        CurrentNumberOfCopiesReceived = 0;
    }

}
