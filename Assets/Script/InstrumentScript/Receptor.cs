﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(BoxCollider2D))]
public class Receptor : Instrument, ITriggerCycle, ITriggerClear
{
    [SerializeField] int numberOfRequiredCopies = 20;

    private List<SpatialMode> _enteredModeList;
    private bool _isWon = false;
    private TMP_Text _counterText;
    private int _currentNumberOfCopiesReceived = 0;
    private bool _isSubscribed = false;

    internal TMP_Text CounterText
    {
        get
        {
            if (_counterText == null)
                _counterText = associatedTicket.GetComponentInChildren<TMP_Text>();
            return (_counterText);
        }
    }
    internal int CurrentNumberOfCopiesReceived
    {
        get { return (_currentNumberOfCopiesReceived); }
        set
        {
            _currentNumberOfCopiesReceived = value;
            CounterText.text = _currentNumberOfCopiesReceived.ToString() + "/" + numberOfRequiredCopies.ToString();
        }
    }


    public override void InitInstrument()
    {
        _enteredModeList = new List<SpatialMode>();
        SubscribeClear();
    }

    public bool CleanEnteredMode()
    {
        int i = 0;
        while (i < _enteredModeList.Count)
        {
            if (_enteredModeList[i].isDestroyed == true)
                _enteredModeList.RemoveAt(i);
            else
                i = i + 1;
        }
        if (_enteredModeList.Count == 0)
        {
            UnsubscribeCycle();
            return (false);
        }
        return (true);
    }

    public void TriggerCycle()
    {
        if (!CleanEnteredMode())
            return;

        // We check if we have all the modes of this specific faketon and only act when it's the case
        if (_enteredModeList[0].owner.numberOfSpatialModes == _enteredModeList.Count)
        {
            Faketon commonOwner;

            commonOwner = _enteredModeList[0].owner;
            if (Faketon.IsEqual(commonOwner, associatedFaketon))
            {
                if (CurrentNumberOfCopiesReceived < numberOfRequiredCopies)
                {
                    CurrentNumberOfCopiesReceived += 1;
                    if (CurrentNumberOfCopiesReceived == numberOfRequiredCopies / 4)
                        _levelManager.QuarterReceptor();
                    else if (CurrentNumberOfCopiesReceived == numberOfRequiredCopies / 2)
                        _levelManager.HalfReceptor();
                    else if (!_isWon && CurrentNumberOfCopiesReceived == numberOfRequiredCopies)
                    {
                        _isWon = true;
                        _levelManager.WinReceptor();
                    }
                }

            }
            else
            {
				RaiseWrongFaketon();
            }
            Faketon.DestroyTotally(commonOwner);
            _enteredModeList.Clear();
            UnsubscribeCycle();
        }
    }

    /// <summary>
    /// When a new SpatialMode enter, we unsubscribe it and remove it from the collision
    /// we check wether the Faketon owner is the same as the owner of previously entered
    /// mode. We raise an error if it don't fits.
    /// </summary>
    /// <param name="collision"></param>
    public override void OnTriggerEnter2D(Collider2D collision)
    {
        CleanEnteredMode();
        collision.gameObject.TryGetComponent<SpatialMode>(out var newlyEnteredSpatialMode);
        if (newlyEnteredSpatialMode)
        {
            newlyEnteredSpatialMode.Unsubscribe();
            newlyEnteredSpatialMode.AddInstrument(this);
            if (_enteredModeList.Count == 0)
            {
                _enteredModeList.Add(newlyEnteredSpatialMode);
                SubscribeCycle();
            }
            else if (newlyEnteredSpatialMode.owner == _enteredModeList[0].owner)
            {
                _enteredModeList.Add(newlyEnteredSpatialMode);
                if (_enteredModeList.Count > associatedFaketon.numberOfSpatialModes)
                    RaiseWrongFaketon();
            }
            else
            {
                RaiseWrongFaketon();
            }
            
        }
    }

	private void RaiseWrongFaketon()
	{
		_levelManager.Error(transform.position, ErrorType.WrongFaketon);
	}

    public void SubscribeCycle()
    {
        if (!_isSubscribed)
        {
            _isSubscribed = true;
            _levelManager.StartReceptorCycle += TriggerCycle;
        }
        
    }

    public void UnsubscribeCycle()
    {
        if (_isSubscribed)
        {
            _isSubscribed = false;
            _levelManager.StartReceptorCycle -= TriggerCycle;
        }
        
    }

    public void SubscribeClear()
    {
        _levelManager.ClearEvent += TriggerClear;
    }

    public void UnsubscribeClear()
    {
        _levelManager.ClearEvent -= TriggerClear;
    }

    public void TriggerClear()
    {
        UnsubscribeCycle();
        _enteredModeList.Clear();
        _isWon = false;
        CurrentNumberOfCopiesReceived = 0;
    }

    public override void SetSpecificData(int[] specificData)
    {
        //throw new System.NotImplementedException();
    }
}
