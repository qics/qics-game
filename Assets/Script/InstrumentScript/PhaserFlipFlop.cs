﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class PhaserFlipFlop : Instrument, ITriggerClear, ITriggerCycle, IClassicalReceptor
{
    private int _elapsedCycle = 0;
    private Connector _connector;
    private SpatialMode newlyEnteredSpatialMode;
    private bool _isTriggered = false;
    private List<SpatialMode> _inputModes;
    private bool _alreadySubscribe = false;
    private int _onOffset = 0;
    private int _prevCycle;
    [SerializeField] Slider _frequencySlider;
    [SerializeField] Slider _initialStateSlider;
    [SerializeField] Sprite[] _cadrantsSprite;
    [SerializeField] SpriteRenderer _rendererCadrant;


    public SpriteRenderer spriteRenderer;
    public int currentPhase = -1;
    public int numberOfCycleToChange = -1;


    public override void InitInstrument()
    {
        _connector = GetComponentInChildren<Connector>();
        _inputModes = new List<SpatialMode>();
        _specificInstruData = new int[2];
        if (_frequencySlider == null || _initialStateSlider == null)
            Debug.LogError("Error: Slider unassigned to object " + name);
        else
        {
            _frequencySlider.InitSlider(this);
            _initialStateSlider.InitSlider(this);
        }
        SubscribeClear();
        SubscribeCycle();
    }

    public void TriggerCycle()
    {
        int i = 0;

        while (i < _inputModes.Count)
        {
            if (_inputModes[i].isDestroyed == true)
                _inputModes.RemoveAt(i);
            else
                i = i + 1;
        }
        if (numberOfCycleToChange > 0)
        {
            _elapsedCycle += 1;
            ChangeSprite(numberOfCycleToChange - _elapsedCycle + _onOffset);
            if (_elapsedCycle == numberOfCycleToChange)
            {
                _elapsedCycle = 0;

                if (currentPhase == -1)
                    SwitchPhaserPhase(0);
                else
                    SwitchPhaserPhase(1);
            }

            if (_isTriggered && currentPhase == -1)
            {

                foreach (SpatialMode mode in _inputModes)
                {
                    Faketon owner;
                    int indexMode;

                    owner = mode.owner;
                    indexMode = owner.spatialModes.IndexOf(mode);
                    foreach (State state in owner.stateList)
                    {
                        if (state.numberOfQubits[indexMode] % 2 == 1)
                            state.SwitchPhase(currentPhase);
                    }
                    if (mode != null)
                    {
                        mode.owner.DrawFaketon();
                        mode.TriggerCycle();
                        mode.Subscribe();
                    }
                }
            }
            else
            {
                foreach (SpatialMode mode in _inputModes)
                {
                    if (mode != null)
                    {
                        mode.owner.DrawFaketon();
                        mode.TriggerCycle();
                        mode.Subscribe();
                    }
                }
            }


        }
        else
        {
            if (_isTriggered && currentPhase == -1)
            {
                foreach (SpatialMode mode in _inputModes)
                {
                    Faketon owner;
                    int indexMode;

                    owner = mode.owner;
                    indexMode = owner.spatialModes.IndexOf(mode);
                    foreach (State state in owner.stateList)
                    {
                        if (state.numberOfQubits[indexMode] % 2 == 1)
                            state.SwitchPhase(currentPhase);
                    }
                    if (mode != null)
                    {
                        mode.owner.DrawFaketon();
                        mode.TriggerCycle();
                        mode.Subscribe();
                    }
                }
                UnsubscribeCycle();
            }
            else
            {
                foreach (SpatialMode mode in _inputModes)
                {
                    if (mode != null)
                    {
                        mode.owner.DrawFaketon();
                        mode.TriggerCycle();
                        mode.Subscribe();
                    }
                }
            }

        }
        _isTriggered = false;
        _inputModes.Clear();

    }


    public void SetIsControlled(bool isControlled)
    {
        if (isControlled)
        {
            _prevCycle = numberOfCycleToChange;
            _frequencySlider.ChangeValue(-1);
            _frequencySlider.IsInteractable = false;
            UnsubscribeCycle();
        }
        else
        {
            _frequencySlider.IsInteractable = true;
            if (_prevCycle > 0)
                _frequencySlider.ChangeValue(_prevCycle);
            else
                _frequencySlider.ChangeValue(3);
            SubscribeCycle();
        }
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {

        newlyEnteredSpatialMode = collision.gameObject.GetComponent<SpatialMode>();

        if (!_isTriggered)
        {
            _isTriggered = true;
            if (numberOfCycleToChange <= 0)
                SubscribeCycle();
        }
        _inputModes.Add(newlyEnteredSpatialMode);
        newlyEnteredSpatialMode.Unsubscribe();

    }

    public void ReceiveSignal(bool signal)
    {
        if (signal == true)
        {
            if (currentPhase == 1)
                SwitchPhaserPhase(1);
            else
                SwitchPhaserPhase(0);
        }

    }

    public override void BeginDrag(Vector3 position, Vector2 mousePosition)
    {
        _initialPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        spriteObject.sortingLayerName = "FloatingObjects";
        _rendererCadrant.sortingLayerName = "FloatingObjects";
        _levelManager.gridHandler.FreeCell(transform.position);
        transform.Translate(Vector3.forward * -5f);
        noDrag = true;
    }

    public override void EndDrag(Vector3 position, Vector2 mousePosition)
    {
        Vector2Int gridPosition = _levelManager.gridHandler.WorldToGridCoord(position);
        Ray inputRay = Camera.main.ScreenPointToRay(mousePosition);
        RaycastHit2D hitResults = new RaycastHit2D();

        spriteObject.sortingLayerName = "Instruments";
        _rendererCadrant.sortingLayerName = "Instruments";
        transform.Translate(Vector3.forward * 5f);
        hitResults = Physics2D.Raycast(inputRay.origin, inputRay.direction, 1000f, LayerMask.GetMask("GameUI"));
        if (!isDraggable || _levelManager.gridHandler.IsLocked(gridPosition) || hitResults.transform != null)
        {
            if (isOnGrid == false || hitResults.transform != null)
                Instrument.Destroy(this);
            else
            {
                transform.position = (Vector3)_initialPosition;
                _levelManager.gridHandler.LockCell((Vector3)_initialPosition, this);
            }

        }
        else
        {
            transform.position = _levelManager.gridHandler.NearestGridCenter(gridPosition);
            Vector2Int initialGrid = _levelManager.gridHandler.WorldToGridCoord((Vector3)_initialPosition);
            if (isOnGrid && initialGrid != gridPosition)
                _historyManager.AddMoveInstrumentAction(gridPosition, initialGrid);
            else if (isOnGrid == false)
                _historyManager.AddSpawnInstrumentAction(gridPosition);
            _levelManager.gridHandler.LockCell(gridPosition, this);
        }
        if (noDrag)
        {
            if (!InputManager.IsSelected(gameObject))
            {
                InputManager.ClearSelection();
                selector.SetActive(true);
                InputManager.AddToSelection(gameObject);
            }
            else
                InputManager.ClearSelection();
        }
    }


    public void SwitchPhaserPhase(int newPhase)
    {
        if (newPhase == 0)
        {
            //spriteRenderer.sprite = offSprite;
            _onOffset = 0;
            currentPhase = 1;
        }
        else
        {
            //spriteRenderer.sprite = onSprite;
            _onOffset = 8;
            currentPhase = -1;
        }
        if (numberOfCycleToChange == -1)
            ChangeSprite(7 + _onOffset);
        else
            ChangeSprite(numberOfCycleToChange + _onOffset);
    }

    public void ChangeFrequency(int newValue)
    {
        _specificInstruData[0] = newValue;
        numberOfCycleToChange = newValue;
        if (newValue == -1)
            ChangeSprite(7 + _onOffset);
        else
            ChangeSprite(numberOfCycleToChange + _onOffset);
    }

    private void ChangeSprite(int position)
    {
        _rendererCadrant.sprite = _cadrantsSprite[position];
    }

    public void ChangeInitialState(int newState)
    {
        _specificInstruData[1] = newState;
        SwitchPhaserPhase(newState);
    }

    public void SubscribeCycle()
    {
        if (!_alreadySubscribe)
        {
            _alreadySubscribe = true;
            _levelManager.StartCycle += TriggerCycle;
        }

    }

    public void UnsubscribeCycle()
    {
        if (_alreadySubscribe)
        {
            _alreadySubscribe = false;
            _levelManager.StartCycle -= TriggerCycle;
        }
    }

    public void SubscribeClear()
    {
        _levelManager.ClearEvent += TriggerClear;
    }

    public void UnsubscribeClear()
    {
        _levelManager.ClearEvent -= TriggerClear;
    }

    public override void SetSpecificData(int[] specificData)
    {
        if (specificData == null)
            Debug.LogError("Wesh, les datas ne sont pas init, y'a un prob");
        _frequencySlider.ChangeValue(specificData[0]);
        _initialStateSlider.ChangeValueBool(specificData[1]);
    }

    public void TriggerClear()
    {
        _elapsedCycle = 0;
        if (numberOfCycleToChange <= 0)
            UnsubscribeCycle();
        _isTriggered = false;
        _inputModes.Clear();
        SwitchPhaserPhase(_specificInstruData[1]);
        if (numberOfCycleToChange == -1)
            ChangeSprite(7 + _onOffset);
        else
        {
            SubscribeCycle();
            _elapsedCycle = -1;
            ChangeSprite(numberOfCycleToChange + _onOffset);
        }
            
    }
}
