using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonPhysicsBell : Instrument, ITriggerCycle, IClassicalEmitter
{
    private List<SpatialMode> _inputModes;
    private bool _isTriggered = false;
    private Connector _connector;
    [SerializeField] Slider _numberMeasuredSlider;
    [SerializeField] Slider _destructiveSlider;

    public int numberMeasured = 2;
    public bool destructiveMeasurement = false;

    public override void InitInstrument()
    {
        _inputModes = new List<SpatialMode>();
        _specificInstruData = new int[2];
        if (_numberMeasuredSlider == null || _destructiveSlider == null)
            Debug.LogError("Error: Slider unassigned to object " + name);
        else
        {
            _numberMeasuredSlider.InitSlider(this);
            _destructiveSlider.InitSlider(this);
        }
        _connector = GetComponentInChildren<Connector>();
        SubscribeClear();
    }

    public void TriggerCycle()
    {
        int i = 0;
        while (i < _inputModes.Count)
        {
            if (_inputModes[i].isDestroyed == true)
                _inputModes.RemoveAt(i);
            else
                i = i + 1;
        }
        if (_inputModes.Count == 0)
            return;
        bool result = MeasureModes(_inputModes);
        SendSignal(result);
        foreach (SpatialMode mode in _inputModes)
        {
            if (mode != null)
            {
                mode.TriggerCycle();
                mode.Subscribe();
            }
        }
        _inputModes.Clear();
        _isTriggered = false;
        UnsubscribeCycle();


    }

    public void SendSignal(bool signal)
    {
        _connector.SendSignal(signal);
    }

    private bool MeasureModes(List<SpatialMode> modes)
    {
        Faketon currentOwner;
        float maxProbaSuccess = 0f;
        List<State> newListStateSuccess = new List<State>();
        float maxProbaFailure = 0f;
        List<State> newListStateFailure = new List<State>();
        List<int> indexOfModes = new List<int>();

        currentOwner = modes[0].owner;
        currentOwner.transform.parent = _levelManager.levelGrid.transform;
        for (int i = 1; i < modes.Count; i++)
        {
            if (modes[i].owner != currentOwner)
                currentOwner = Faketon.Fuse(currentOwner, modes[i].owner);
        }
        currentOwner.Print();
        foreach (SpatialMode mode in modes)
        {
            indexOfModes.Add(currentOwner.spatialModes.IndexOf(mode));
        }


        foreach (State state in currentOwner.stateList)
        {
            int numberOfQubit = 0;


            foreach (int index in indexOfModes)
            {
                numberOfQubit += state.numberOfQubits[index];
            }


            if (numberOfQubit == numberMeasured)
            {
                State newState = new State();

                newState.proba = state.proba;
                newState.phase = state.phase;
                if (destructiveMeasurement)
                {
                    int k = 0;

                    newState.numberOfQubits = new int[currentOwner.numberOfSpatialModes - modes.Count];
                    for (int i = 0; i < currentOwner.numberOfSpatialModes; i++)
                    {
                        if (indexOfModes.Contains(i))
                            k += 1;
                        else
                            newState.numberOfQubits[i - k] = state.numberOfQubits[i];
                    }
                }
                else
                {
                    newState.numberOfQubits = new int[currentOwner.numberOfSpatialModes];
                    for (int i = 0; i < currentOwner.numberOfSpatialModes; i++)
                    {
                        newState.numberOfQubits[i] = state.numberOfQubits[i];
                    }
                }
                int prevIndex = Faketon.GetIndexOfState(newListStateSuccess, newState.numberOfQubits);
                if (prevIndex >= 0)
                {
                    State prevState = newListStateSuccess[prevIndex];
                    maxProbaSuccess -= prevState.proba;
                    if (prevState.phase == newState.phase)
                        prevState.proba = Mathf.Pow(Mathf.Sqrt(prevState.proba) + Mathf.Sqrt(newState.proba), 2);
                    else
                    {
                        if (prevState.proba > newState.proba)
                            prevState.proba = Mathf.Pow(Mathf.Sqrt(prevState.proba) - Mathf.Sqrt(newState.proba), 2);
                        else
                        {
                            prevState.phase = newState.phase;
                            prevState.proba = Mathf.Pow(-Mathf.Sqrt(prevState.proba) + Mathf.Sqrt(newState.proba), 2);
                        }

                    }
                    if (prevState.proba < 0.001f)
                        newListStateSuccess.RemoveAt(prevIndex);
                    else
                    {
                        newListStateSuccess[prevIndex] = prevState;
                        maxProbaSuccess += prevState.proba;
                    }
                }
                else
                {
                    maxProbaSuccess += newState.proba;
                    newListStateSuccess.Add(newState);
                }

            }
            else
            {
                State newState = new State();
                newState.proba = state.proba;
                newState.phase = state.phase;
                if (destructiveMeasurement)
                {
                    int k = 0;

                    newState.numberOfQubits = new int[currentOwner.numberOfSpatialModes - modes.Count];
                    for (int i = 0; i < currentOwner.numberOfSpatialModes; i++)
                    {
                        if (indexOfModes.Contains(i))
                            k += 1;
                        else
                            newState.numberOfQubits[i - k] = state.numberOfQubits[i];
                    }
                }
                else
                {
                    newState.numberOfQubits = new int[currentOwner.numberOfSpatialModes];
                    for (int i = 0; i < currentOwner.numberOfSpatialModes; i++)
                    {
                        newState.numberOfQubits[i] = state.numberOfQubits[i];
                    }
                }
                int prevIndex = Faketon.GetIndexOfState(newListStateFailure, newState.numberOfQubits);
                if (prevIndex >= 0)
                {
                    State prevState = newListStateFailure[prevIndex];
                    maxProbaFailure -= prevState.proba;
                    if (prevState.phase == newState.phase)
                        prevState.proba = Mathf.Pow(Mathf.Sqrt(prevState.proba) + Mathf.Sqrt(newState.proba), 2);
                    else
                    {
                        if (prevState.proba > newState.proba)
                            prevState.proba = Mathf.Pow(Mathf.Sqrt(prevState.proba) - Mathf.Sqrt(newState.proba), 2);
                        else
                        {
                            prevState.phase = newState.phase;
                            prevState.proba = Mathf.Pow(-Mathf.Sqrt(prevState.proba) + Mathf.Sqrt(newState.proba), 2);
                        }

                    }
                    if (prevState.proba < 0.001f)
                        newListStateFailure.RemoveAt(prevIndex);
                    else
                    {
                        newListStateFailure[prevIndex] = prevState;
                        maxProbaFailure += prevState.proba;
                    }
                }
                else
                {
                    maxProbaFailure += newState.proba;
                    newListStateFailure.Add(newState);
                }
            }
        }
        float dice = Random.Range(0f, maxProbaSuccess + maxProbaFailure);
        if (dice <= maxProbaSuccess)
        {
            for (int i = 0; i < newListStateSuccess.Count; i++)
            {
                newListStateSuccess[i].proba = newListStateSuccess[i].proba / maxProbaSuccess;
            }
            currentOwner.stateList = newListStateSuccess;
            if (destructiveMeasurement)
            {
                currentOwner.numberOfSpatialModes -= modes.Count;
                while (modes.Count > 0)
                {
                    SpatialMode modeToDestroy = modes[0];
                    modes.RemoveAt(0);
                    currentOwner.spatialModes.Remove(modeToDestroy);
                    SpatialMode.Destroy(modeToDestroy);
                }
                if (currentOwner.numberOfSpatialModes == 0)
                {
                    Faketon.Destroy(currentOwner);
                    return (true);
                }
            }
            currentOwner.CleanEmptyModes();
            Faketon.SeparateFaketon(currentOwner);
            return (true);
        }
        else
        {
            for (int i = 0; i < newListStateFailure.Count; i++)
            {
                newListStateFailure[i].proba = newListStateFailure[i].proba / maxProbaFailure;
            }
            currentOwner.stateList = newListStateFailure;
            if (destructiveMeasurement)
            {
                currentOwner.numberOfSpatialModes -= modes.Count;
                while (modes.Count > 0)
                {
                    SpatialMode modeToDestroy = modes[0];
                    modes.RemoveAt(0);
                    currentOwner.spatialModes.Remove(modeToDestroy);
                    SpatialMode.Destroy(modeToDestroy);
                }
                if (currentOwner.numberOfSpatialModes == 0)
                {
                    Faketon.Destroy(currentOwner);
                    return (false);
                }
            }
            currentOwner.CleanEmptyModes();
            Faketon.SeparateFaketon(currentOwner);
            return (false);
        }
    }

    public override void SetSpecificData(int[] specificData)
    {
        _destructiveSlider.ChangeValueBool(specificData[0]);
        _numberMeasuredSlider.ChangeValue(specificData[1]);
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        var newlyEnteredSpatialMode = collision.gameObject.GetComponent<SpatialMode>();
        if (newlyEnteredSpatialMode.IsInCollidingLayer())
        {
            if (!_isTriggered)
            {
                _isTriggered = true;
                SubscribeCycle();
            }
            newlyEnteredSpatialMode.AddInstrument(this);
            newlyEnteredSpatialMode.Unsubscribe();
            _inputModes.Add(newlyEnteredSpatialMode);
        }

    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        SpatialMode mode = collision.GetComponent<SpatialMode>();
        mode.RemoveInstrument(this);
    }


    public void SubscribeCycle()
    {
        _levelManager.StartCycle += TriggerCycle;
    }

    public void UnsubscribeCycle()
    {
        _levelManager.StartCycle -= TriggerCycle;
    }

    public void ChangeMeasureMode(int newValue)
    {
        if (newValue == 0)
        {
            destructiveMeasurement = false;
            _specificInstruData[0] = 0;
        }
        else
        {
            _specificInstruData[0] = 1;
            destructiveMeasurement = true;
        }
            
    }

    public void SubscribeClear()
    {
        _levelManager.ClearEvent += TriggerClear;
    }

    public void UnsubscribeClear()
    {
        _levelManager.ClearEvent -= TriggerClear;
    }

    public void TriggerClear()
    {
        _inputModes.Clear();
        _isTriggered = false;
        UnsubscribeCycle();
    }

    public void ChangeNumberMeasured(int newValue)
    {
        numberMeasured = newValue;
        _specificInstruData[1] = newValue;
    }
}
