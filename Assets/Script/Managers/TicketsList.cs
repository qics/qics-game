using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicketsList : ScriptableObject
{
    [SerializeField] List<InstrumentTicket> instrumentInLevel;
}
