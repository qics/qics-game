using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Page : MonoBehaviour
{
    [SerializeField] Book _book;

    public void OnPageTurned()
    {
        _book.OnPageTurned();
    }

    public void OnPageToTurn()
    {
        _book.OnPageToTurn();
    }

}
