using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using TMPro;

public class MarquePage : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] GameObject _complement;
    [SerializeField] Book _book;
    [SerializeField] TMP_Text _nameOfSection;
    [SerializeField] GameObject _content;

    private CanvasGroup _canvasGroup;

    private UnityEngine.UI.Image _complementImage;

    public CanvasGroup CanvasGroup {get {return (_canvasGroup);}}
    public UnityEngine.UI.Image ComplementImage {get {return (_complementImage);}}

    public void Awake()
    {
        _complementImage = _complement.GetComponent<UnityEngine.UI.Image>();
        _canvasGroup = _content.GetComponent<CanvasGroup>();
        if (_book == null)
            _book = GetComponentInParent<Book>();
    }

    public void ActivateComplement()
    {
        _complement.SetActive(true);
    }

    public void DisableComplement()
    {
        _complement.SetActive(false);
    }

    public void ClickMarquePage()
    {
        _book.TurnPage(this);
    }



     public void OnPointerEnter(PointerEventData eventData)
     {
         _complementImage.color = new Color(0.3f,0.3f,0.3f, _complementImage.color.a);
         _nameOfSection.gameObject.SetActive(true);
     }

    public void OnPointerExit(PointerEventData eventData)
    {
        _complementImage.color = new Color(0.4f,0.4f,0.4f,  _complementImage.color.a);
        _nameOfSection.gameObject.SetActive(false);
    }
}
