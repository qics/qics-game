﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuManager : MonoBehaviour
{
    [SerializeField] ToggleGroup _languageToggle = null;
    [SerializeField] TMP_Text _cheatText;
    
    private GameManager _gameManager = null;

    public void Awake()
    {
        _gameManager = FindObjectOfType<GameManager>();
        if (_languageToggle == null)
        {
            Debug.LogWarning("Toggle Group not initiated in MenuManager have to look for it.");
            _languageToggle = FindObjectOfType<ToggleGroup>();
        }



        if (!PlayerPrefs.HasKey("Language"))
        {
            PlayerPrefs.SetInt("Language", (int)Language.English);
        }
        _gameManager.CurrentLanguage = (Language)PlayerPrefs.GetInt("Language");

        IEnumerable<Toggle> _languages = _languageToggle.GetComponentsInChildren<Toggle>();
        foreach (Toggle _language in _languages)
        {
            if (_language.name == _gameManager.CurrentLanguage.ToString())
                _language.isOn = true;
        }
        _languageToggle.allowSwitchOff = false;

    }

    public void StartGame()
    {
        SceneManager.LoadScene("Lvl1", LoadSceneMode.Single);
    }

    public void LevelsMenu()
    {
        SceneManager.LoadScene("MenuLevels", LoadSceneMode.Single);
    }

    public void CreditScene()
    {
        SceneManager.LoadScene("CreditScene", LoadSceneMode.Single);
    }

    public void ResetSave()
    {
        GameManager gameManager = GameManager.Instance;
        gameManager.ResetSave();
    }

    private int _secretCounter = 0;

    public void Unlock()
    {
        _secretCounter++;
        if (_secretCounter >= 5)
        {
            GameManager gameManager = GameManager.Instance;
            gameManager.DEBUG_cheatMode = true;
            _cheatText.gameObject.SetActive(true);
        }

    }

    public void ChooseFrenchLanguage(bool isOn)
    {
        if (isOn)
        {
            _gameManager.CurrentLanguage = Language.French;
            PlayerPrefs.SetInt("Language", (int)Language.French);
        }

    }

    public void ChooseEnglishLanguage(bool isOn)
    {
        if (isOn)
        {
            _gameManager.CurrentLanguage = Language.English;
            PlayerPrefs.SetInt("Language", (int)Language.English);
        }
    }

    public void ChangeTheme(ThemeSpatialMode newTheme)
    {
        _gameManager.CurrentThemeSpatialMode = newTheme;
    }


}
