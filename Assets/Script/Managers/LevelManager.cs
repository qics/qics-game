﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR.Haptics;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;
using UnityEngine.Networking;
using System.IO;
using System;
using UnityEngine.Experimental.Rendering.Universal;
using System.Globalization;

enum LevelState
{
    Cleared,
    Paused,
    Running,
    UI,
    Won,
    Book,
    Error
}

public enum ErrorType
{
	Collision,
	WrongFaketon,
	TooMuchModes,
	TooMuchStates,
    OutOfSimulation,
    SeveralOwner,
    TooMuchFaketon,
    TwoDifferentState
}

//Maybe Level Manager Injection

/// <summary>
/// Handle each level. Is in charge of managing that the level run as it should.
/// </summary>
public class LevelManager : Singleton<LevelManager>
{
    #region Fields

    [SerializeField] GameObject _inputManagerConfig;
    private IInputManager _inputManager = null;

    [SerializeField] GameObject _dialogueManagerConfig;
    private IDialogueManager _dialogueManager = null;

    [SerializeField] GameObject _displayerManagerConfig;
    private IDisplayerManager _displayerManager = null;

    [SerializeField] ScriptableObject _dataManagerConfig = null;
    private ILevelDataManager _levelDataManager = null;

    [SerializeField] ScriptableObject _threadManagerConfig = null;
    private IThreadManager _threadManager;

    [SerializeField] ScriptableObject _historyManagerConfig = null;

    private IHistoryManager _historyManager;

    [SerializeField] GameObject _book;

    [SerializeField] GameObject UIparent;
    [SerializeField] Transform _displayerParent;
    [SerializeField] GameObject _resultsPrinterObj;
    [SerializeField] Transform _ticketsParent;
    [SerializeField] TMP_Text _cycleCounterText;
    [SerializeField] UnityEngine.UI.Toggle _classicalToggle;
    [SerializeField] float _normalTimeBetweenCycles = 0.6f;
    [SerializeField] float _fastTimeBetweenCycles = 0.2f;
    [SerializeField] GameObject _helpPanel;
    [SerializeField] GameObject _keyPanel;
    [SerializeField] int _maxLinearCombination = 60;
    [SerializeField] int _maxSpatialModes = 10;
    [SerializeField] ErrorText _errorText;
    [SerializeField] List<EmotionsSprite> _playerSprites;
    [SerializeField] List<EmotionsSprite> _profSprites;

    public int MaxLenghtOfLinearCombination { get { return (_maxLinearCombination); } }
    public int MaxNumberOfSpatialModes { get { return (_maxSpatialModes); } }
    public bool IsAmplitude {get; set;} = false;
    private IResultPrinter _resultsPrinter;
    private float _timeSinceLastCycle = 0;
    private LevelState _currentLevelState;
    public float _nextTimeBetweenCycles;
    private float _minTime;
    private List<Faketon> _emittedFaketon;
    private int _numberOfWonReceptor = 0;
    private int _numberOfQuarterReceptor = 0;
    private SpriteMask _spriteMask;
    private int _cycleCounter = 0;
    private int _randomSeed;
    private bool _oneCycle = false;
    public bool IsInClassicalMode { get; set; } = false;
    private float _timeBetweenCycles = 1f;
    private GameManager _gameManager = null;
    private string _currentLevel;
    private const string highscoreURL = "https://qicsgameserver.000webhostapp.com/leaderboard.php";

    public List<Faketon> EmittedFaketon
    {
        get
        {
            if (_emittedFaketon == null)
                _emittedFaketon = new List<Faketon>();
            return (_emittedFaketon);
        }
    }
    public int CycleCounter
    {
        get { return (_cycleCounter); }
        set
        {
            _cycleCounter = value;
            _cycleCounterText.text = _cycleCounter.ToString();
        }
    }
    public UnityEngine.UI.Toggle ClassicalToggle { get { return (_classicalToggle); } }

    public float TimeBetweenCycles
    {
        get { return (_timeBetweenCycles); }
        private set
        {
            _timeBetweenCycles = value;
            currentVelocity = levelGrid.cellSize.x / _timeBetweenCycles;
        }
    }
    public Transform TicketParent { get { return (_ticketsParent); } }
    public Transform DisplayerParent { get { return (_displayerParent); } }
    public GameManager GameManager
    {
        get 
        {
            if (_gameManager == null)
                _gameManager = GameManager.Instance;
            return (_gameManager);
        }
    }
    public delegate void StartCycleHandler();
    public event StartCycleHandler StartCycle;
    public delegate void StartClassicalCycleHandler();
    public event StartClassicalCycleHandler StartClassicalCycle;
    public delegate void StartReceptorCycleHandler();
    public event StartReceptorCycleHandler StartReceptorCycle;
    public delegate void ClearEventHandler();
    public event ClearEventHandler ClearEvent;
    public delegate void SwitchClassicalViewHandler();
    public event SwitchClassicalViewHandler ClassicalViewEvent;
    public Grid levelGrid;
    public GridHandler gridHandler;
    public GameObject crossGameObject;
    public GameObject nextButtonGameObject;
    public GameObject threadsParent;
    public int numberOfReceptor = 1;
    public string nextLevelSceneName = null;
    public ThemeSpatialMode themeSpatialMode;
    public UserData userData;
    public float currentVelocity;

    #endregion

    /// <summary>
    /// Awake method are called when the object activate for the first time.
    /// The instance part come from the fact that LevelManager is a Singleton.
    /// </summary>
    protected override void Awake()
    {
        userData = GameManager.userData;
        levelGrid = GetComponentInChildren<Grid>();

        _currentLevel = SceneManager.GetActiveScene().name;
        gridHandler = levelGrid.GetComponent<GridHandler>();
        gridHandler.SetTickets(UIparent.transform.parent.GetComponentsInChildren<InstrumentTicket>());
        gridHandler.Init();

        //_minTime = Time.fixedDeltaTime * 2f;
        _randomSeed = Animator.StringToHash(SceneManager.GetActiveScene().name);
        _timeBetweenCycles = _normalTimeBetweenCycles;
        _nextTimeBetweenCycles = _normalTimeBetweenCycles;
        _currentLevelState = LevelState.Cleared;
        _spriteMask = GetComponentInChildren<SpriteMask>();
        _spriteMask.gameObject.SetActive(false);

        if (instance == null)
        {
            instance = this as LevelManager;
        }

        _historyManager = (IHistoryManager)_historyManagerConfig;
        if (_historyManager == null)
            Debug.LogError("No history managing system found");
        else
            _historyManager.Init();


        if (_displayerManagerConfig == null || !_displayerManagerConfig.TryGetComponent<IDisplayerManager>(out var temp2))
        {
            Debug.LogError("No state displayer managing system found");
        }
        else
        {
            _displayerManager = (Instantiate(_displayerManagerConfig, _displayerParent)).GetComponent<IDisplayerManager>();
            _displayerManager.Init();
        }


        if (_inputManagerConfig == null || !_inputManagerConfig.TryGetComponent<IInputManager>(out var temp))
        {
            Debug.LogError("No Input managing system found");
        }
        else
        {
            _inputManager = (Instantiate(_inputManagerConfig, transform)).GetComponent<IInputManager>();
            _inputManager.Init();
        }

        if (_dialogueManagerConfig != null && _dialogueManagerConfig.TryGetComponent<IDialogueManager>(out var dialogueTemp))
        {
            if (UIparent != null)
                _dialogueManager = (Instantiate(_dialogueManagerConfig, UIparent.transform)).GetComponent<IDialogueManager>();
            else
                _dialogueManager = (Instantiate(_dialogueManagerConfig, transform)).GetComponent<IDialogueManager>();
        }
        _threadManager = (IThreadManager)_threadManagerConfig;
        if (_threadManager == null)
            Debug.LogError("No thread managing system found");
        else
            _threadManager.Init();


        _levelDataManager = (ILevelDataManager)_dataManagerConfig;
        if (_levelDataManager == null)
            Debug.LogError("No Data managing system found");
        else
            _levelDataManager.Init();

        if (!_resultsPrinterObj.TryGetComponent(out _resultsPrinter))
            Debug.LogWarning("No result Printer");
        LoadLevel();
        //DEBUG
        TestingTheTest();
    }

    #region Level data handling

    public LevelData GetLevelInfo()
    {
        return (_levelDataManager.GetLevelData());
    }

    public void LoadLevel(LevelData levelData)
    {
        _levelDataManager.SetLevel(levelData);
        Stop();
    }

    public void SaveLevel()
    {
        LevelData data = _levelDataManager.GetLevelData();
        if (GameManager.userData.levelDatas.ContainsKey(_currentLevel))
            GameManager.userData.levelDatas[_currentLevel] = data;
        else
            GameManager.userData.levelDatas.Add(_currentLevel, data);
    }

    public ThemeSpatialMode GetCurrentTheme()
    {
        ThemeSpatialMode theme = GameManager.CurrentThemeSpatialMode;

        if (theme == null)
            return (themeSpatialMode);
        else
            return (theme);
    }
    public void LoadLevel()
    {
        LevelData data;

        if (GameManager.userData.levelDatas.ContainsKey(_currentLevel))
        {
            data = GameManager.userData.levelDatas[_currentLevel];
            _levelDataManager.SetLevel(data);
        }
        else
        {
            LaunchDialogue();
        }

    }


    public List<EmotionsSprite> GetPlayerEmotions()
    {
        return (_playerSprites);
    }

    public List<EmotionsSprite> GetProfEmotions()
    {
        return (_profSprites);
    }

    #endregion

    #region Level State

    public bool IsLevelStateCleared()
    {
        if (_currentLevelState == LevelState.Cleared)
            return (true);
        else
            return (false);
    }

    public bool IsLevelStateUI()
    {
        if (_currentLevelState == LevelState.UI)
            return (true);
        else
            return (false);
    }

    public bool IsLevelStatePaused()
    {
        if (_currentLevelState == LevelState.Paused)
            return (true);
        else
            return (false);
    }
    public bool IsLevelStateWon()
    {
        return (_currentLevelState == LevelState.Won);
    }

    public bool IsLevelStateBook()
    {
        return (_currentLevelState == LevelState.Book);
    }

    public void SwitchLevelStateToClear()
    {
        _currentLevelState = LevelState.Cleared;
    }

    public void SwitchLevelStateToPause()
    {
        _currentLevelState = LevelState.Paused;
    }


    public void SwitchLevelStateToUI()
    {
        _currentLevelState = LevelState.UI;
    }

    public void SwitchLevelStateToWon()
    {
        _currentLevelState = LevelState.Won;
    }
    
    public void SwitchLevelStateToBook()
    {
        _currentLevelState = LevelState.Book;
    }

    public void TestingTheTest()
    {

    }

    public void LaunchDialogue()
    {
        if (IsLevelStateCleared())
        {
            SwitchLevelStateToUI();
            _dialogueManager.LaunchDialogue();
        }

    }

    public void EndDialogue()
    {
        SwitchLevelStateToClear();
    }

    #endregion

    #region Book Handling

    private LevelState _stateBeforeBook;

    public void OpenCloseBook()
    {
        if ((IsLevelStateCleared() || IsLevelStatePaused()) && !_book.activeInHierarchy)
            OpenBook();
        else if (IsLevelStateBook())
            CloseBook();
    }

    public void OpenBook()
    {
        _book.SetActive(true);
        _stateBeforeBook = _currentLevelState;
        SwitchLevelStateToBook();
        _inputManager.ClearSelection();
    }

    public void CloseBook()
    {
        _book.SetActive(false);
        _currentLevelState = _stateBeforeBook;

    }

    #endregion

    #region Cycle Utility
    /// <summary>
    /// The LevelManager manage execution flow. Every TimeBetweenCycles, launch a new cycle by calling FireCycle.
    /// </summary>
    /*private void Update()
    {
        if (_currentLevelState == LevelState.Running)
        {
            _timeSinceLastCycle += Time.deltaTime;
            if (_timeSinceLastCycle > TimeBetweenCycles)
            {
                _timeSinceLastCycle -= TimeBetweenCycles;
                TimeBetweenCycles = _nextTimeBetweenCycles;
                FireCycle();
            }
        }
        foreach (Faketon faketon in EmittedFaketon)
        {
            faketon.DrawLine();
        }

    }*/

    /// <summary>
    /// The LevelManager manage execution flow. Every TimeBetweenCycles, launch a new cycle by calling FireCycle.
    /// </summary>
    private void FixedUpdate()
    {
        if (_currentLevelState == LevelState.Running)
        {
            _timeSinceLastCycle += Time.fixedDeltaTime;
            if (_timeSinceLastCycle > TimeBetweenCycles)
            {
                _timeSinceLastCycle -= TimeBetweenCycles;
                TimeBetweenCycles = _nextTimeBetweenCycles;
                FireCycle();
            }
        }
        foreach (Faketon faketon in EmittedFaketon)
        {
            faketon.DrawLine();
        }

    }


    /// <summary>
    /// Every object which act on every cycle subscribe to StartCycle with a method TriggerCycle.
    /// This method Invoke every subscribed methods. (? check wether StartCycle is null)
    /// </summary>
    private void FireCycle()
    {
        CycleCounter += 1;
        StartClassicalCycle?.Invoke();
        StartCycle?.Invoke();
        StartReceptorCycle?.Invoke();
        if (_oneCycle == true)
        {
            _oneCycle = false;
            Pause();
        }
    }

    #endregion

    #region List of Faketon Utility
    public void AddFaketon(Faketon faketon)
    {
        EmittedFaketon.Add(faketon);
    }

    public void RemoveFaketon(Faketon faketon)
    {
        EmittedFaketon.Remove(faketon);
    }

    #endregion

    /// <summary>
    /// Each level have an assigned number of receptor. During a run those receptor get filled. We speed up the game loop
    /// as each receptor gets filled. This the role of those methods.
    /// </summary>
    #region Win and reception Functions

    public void WinReceptor()
    {
        int[] perf;

        _numberOfWonReceptor += 1;
        if (_numberOfWonReceptor == numberOfReceptor)
        {
            if (_resultsPrinter == null)
            {
                Debug.LogError("Error: No result printer associated.");
            }
            SwitchLevelStateToWon();
            perf = _levelDataManager.SetWin(CycleCounter);

            StartCoroutine(DoPostScores(SceneManager.GetActiveScene().name, "default", perf));
            SaveLevel();
        }
    }

    IEnumerator DoPostScores(string levelName, string id, int[] perf)
    {
        WWWForm form = new WWWForm();
        form.AddField("post_leaderboard", "true");
        form.AddField("levelName", levelName);
        form.AddField("id", id);
        form.AddField("numberOfCycle", perf[0]);
        form.AddField("numberOfInstrument", perf[1]);
        form.AddField("numberOfThread", perf[2]);

        using (UnityWebRequest www = UnityWebRequest.Post(highscoreURL, form))
        {
            www.timeout = 1;
            www.certificateHandler = null;

            yield return www.SendWebRequest();


            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                _resultsPrinter.PrintResults(perf, null);
            }
            else
            {
                Debug.Log("Successfully posted score!");
                float[] globalPerf = new float[3];
                string contents = www.downloadHandler.text;

                int i = 0;
                using (StringReader reader = new StringReader(contents))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {

                        try
                        {
                            globalPerf[i] = float.Parse(line.Substring(0,5), CultureInfo.InvariantCulture);
                            i = i + 1;
                        }
                        catch (Exception e)
                        {
                            Debug.Log("Invalid score: " + e);
                            continue;
                        }
                    }
                }
                _resultsPrinter.PrintResults(perf, globalPerf);
            }
            www.Dispose();
        }

    }


    public void QuarterReceptor()
    {
        _numberOfQuarterReceptor += 1;
        if (_numberOfQuarterReceptor == numberOfReceptor)
        {
            _nextTimeBetweenCycles = _fastTimeBetweenCycles;
        }
    }

    public void HalfReceptor()
    {
        /*_numberOfHalfReceptor += 1;
        if (_numberOfHalfReceptor == numberOfReceptor)
        {
            _nextTimeBetweenCycles /= 2f;
        }*/
    }

    #endregion

    /// <summary>
    /// Each of these methods is linked to an UI button in the level.
    /// </summary>
    #region Button Functions

    ///<summary>
    /// Launch the game loop. 
    /// </summary>
    public void Run(bool isFast = false)
    {
        float newTimeBetweenCycles;

        if (_currentLevelState != LevelState.Error && _currentLevelState != LevelState.Won && !IsLevelStateUI() && !IsLevelStateBook())
        {
        	if (isFast)
            		newTimeBetweenCycles = _fastTimeBetweenCycles;
        	else
            		newTimeBetweenCycles = _normalTimeBetweenCycles;
            _inputManager.ClearSelection();
            if (_currentLevelState == LevelState.Cleared)
            {

                UnityEngine.Random.InitState(_randomSeed);
                ClearEvent?.Invoke();
                TimeBetweenCycles = newTimeBetweenCycles;
                _nextTimeBetweenCycles = newTimeBetweenCycles;
                _timeSinceLastCycle = TimeBetweenCycles;
                _cycleCounter = 0;
            }
            else if (_currentLevelState == LevelState.Running)
            {
                _nextTimeBetweenCycles = newTimeBetweenCycles;
            }
            else if (_currentLevelState == LevelState.Paused)
            {
                TimeBetweenCycles = newTimeBetweenCycles;
                _nextTimeBetweenCycles = newTimeBetweenCycles;
                _timeSinceLastCycle = TimeBetweenCycles;
            }
            _currentLevelState = LevelState.Running;
        }

    }

    /// <summary>
    /// Pause the execution of the game loop if it is running.
    /// </summary>
    public void Pause()
    {
        if (_currentLevelState == LevelState.Running)
        {
            _timeSinceLastCycle = 0;
            _currentLevelState = LevelState.Paused;
        }
    }


    /// <summary>
    /// If an error happened during execution (for now an error is either a bad output or a collision between spatial modes), stop immediatly
    /// the execution and put a red cross at the error location. The only possible action after an error is to clear the game.
    /// </summary>
    /// <param name="location"></param>
    public void Error(Vector3 location, ErrorType error = ErrorType.Collision)
    {
        _currentLevelState = LevelState.Error;
        _errorText.DisplayErrorMessage(error);
        foreach (Faketon faketon in EmittedFaketon)
        {
            foreach (SpatialMode mode in faketon.spatialModes)
            {
                mode.ShouldStop = true;
                mode.StopAllCoroutines();
            }
        }
        crossGameObject.transform.position = new Vector3(location.x, location.y, -5f);
        crossGameObject.SetActive(true);
    }

    public void ActivateHelp(bool active)
    {
        if (_helpPanel != null)
        {
            _helpPanel.SetActive(active);
        }
            
    }

    public void ActivateKey(bool active)
    {
        if (_keyPanel != null)
            _keyPanel.SetActive(active);
    }

    public void OneCycle()
    {
        if (_currentLevelState == LevelState.Paused || _currentLevelState == LevelState.Cleared)
        {
            _oneCycle = true;
            Run(true);
        }

    }

    public void BackToLevelMenu()
    {
        SaveLevel();
        GameManager.LoadLevelScene();
    }

    public void RelaunchDialog()
    {
        if (IsLevelStateCleared())
        {
            SwitchLevelStateToUI();
            _dialogueManager.LoadDialogs();
            _inputManager.ClearSelection();
        }
       
    }

    public void RelaunchTuto()
    {
        if (IsLevelStateCleared())
        {
            SwitchLevelStateToUI();
            _dialogueManager.LoadPanels();
            _inputManager.ClearSelection();
        }
        
    }

    /// <summary>
    /// Stop the game and reset to the initial state.
    /// </summary>
    public void Stop()
    {
        if (_currentLevelState != LevelState.Won && !IsLevelStateUI() && !IsLevelStateBook())
        {
            crossGameObject.SetActive(false);
            CycleCounter = 0;
            _currentLevelState = LevelState.Cleared;
            _numberOfWonReceptor = 0;
            _numberOfQuarterReceptor = 0;
            TimeBetweenCycles = 1f;
            while (EmittedFaketon.Count > 0)
            {
                var faketon = EmittedFaketon[0];
                EmittedFaketon.RemoveAt(0);
                Faketon.DestroyTotally(faketon);
            }
            EmittedFaketon.Clear();
            UnityEngine.Random.InitState(_randomSeed);
            ClearEvent?.Invoke();
        }
    }

    public void NextLevel()
    {
        if (nextLevelSceneName != null)
        {
            SceneManager.LoadScene(nextLevelSceneName);
        }
    }

    public void Faster()
    {
        if (TimeBetweenCycles * 2f / 3f > _minTime)
            _nextTimeBetweenCycles = TimeBetweenCycles * 2f / 3f;
    }
    public void Rotate()
    {
        _inputManager.OnRotateButton();
    }


    public void ClassicalMode(bool isActive)
    {
        if (_inputManager != null)
        {
            _inputManager.ClearSelection();
            ClassicalViewEvent?.Invoke();
            IsInClassicalMode = isActive;
            _spriteMask.gameObject.SetActive(isActive);
        }
        else
        {
            Debug.LogWarning("Unitialized InputManager");
        }
       
    }

    public void AmplitudeDrawing(bool isActive)
    {
        IsAmplitude = IsAmplitude ^ true;
        foreach (Faketon faketon in EmittedFaketon)
        {
            faketon.DrawFaketon();
        }
    }

    public void PrintGrid()
    {
        gridHandler.PrintGrid();
    }
    #endregion


    #region Query
    public void QueryThreadManager(INeedThreadManager client)
    {
        client.SetThreadManager(_threadManager);
    }

    public void QueryInputManager(INeedInputManager client)
    {
        client.SetInputManager(_inputManager);
    }

    public void QueryGridHandler(INeedGridHandler client)
    {
        client.SetGridHandler(gridHandler);
    }

    public void QueryDialogueManager(INeedDialogueManager client)
    {
        client.SetDialogueManager(_dialogueManager);
    }

    public void QueryDisplayerManager(INeedDisplayManager client)
    {
        client.SetDisplayerManager(_displayerManager);
    }

    public void QueryHistoryManager(INeedHistoryManager client)
    {
        client.SetHistoryManager(_historyManager);
    }
    #endregion
}
