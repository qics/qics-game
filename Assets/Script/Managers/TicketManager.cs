﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicketManager : Singleton<TicketManager>
{
	public List<GameObject> ticketObjects;

	protected override void Awake()
	{
		if (instance == null)
		{
			instance = this as TicketManager;
		}
		else
		{
			Destroy(gameObject);
		}
	}
}
