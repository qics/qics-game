using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using UnityEngine;

//TODO: Create a new Thread Manader which Pool from a set of object
[CreateAssetMenu(fileName = "Default Thread Data Manager", menuName = "Managers/Threads/Default")]
public class DefaultThreadManager : ScriptableObject, IThreadManager
{
    [SerializeField] private GameObject threadGameObject;
    private GameObject _threadsParent;

    private LevelManager _levelManager;

    private List<ClassicalThread> _threads;

    private GridHandler _gridHandler;

    public void Init()
    {
        _threads = new List<ClassicalThread>();
        //Very temporary I'm just tired
        _levelManager = LevelManager.Instance;
        _levelManager.QueryGridHandler(this);
    }

    public ClassicalThread InstantiateThread(Vector3 position)
    {
        var newThreadObj = Instantiate(threadGameObject, position, Quaternion.identity, _levelManager.threadsParent.transform);
        ClassicalThread newThread = newThreadObj.GetComponent<ClassicalThread>();
        _threads.Add(newThread);
        return (newThread);
    }

    public void RemoveThread(ClassicalThread thread)
    {
        _threads.Remove(thread);
    }

    public List<ThreadData> GetThreadData()
    {
        List<ThreadData> datas = new List<ThreadData>();

        foreach (ClassicalThread thread in _threads)
        {
            Vector2Int linkedInstrumentsIndex = new Vector2Int(_gridHandler.IndexOfInstrument(thread.ConnectorOne.AssociatedInstrument),
                                                                    _gridHandler.IndexOfInstrument(thread.ConnectorTwo.AssociatedInstrument));
            datas.Add(new ThreadData(linkedInstrumentsIndex, thread.IsNot));
        }

        return (datas);
    }

    public void SetThreads(List<ThreadData> datas)
    {
        foreach (ThreadData data in datas)
        {
            Instrument instrumentOne = _gridHandler.GetInstrumentFromIndex(data.InstruIndex.x);
            Instrument instrumentTwo = _gridHandler.GetInstrumentFromIndex(data.InstruIndex.y);
            PlaceThread(instrumentOne.GetComponentInChildren<Connector>(), instrumentTwo.GetComponentInChildren<Connector>(), data.isNot);
        }
    }

    public void PlaceThread(Connector connectorOne, Connector connectorTwo, bool isNot)
    {
        ClassicalThread newThread = InstantiateThread(Vector3.zero);

        newThread.ConnectorOne = connectorOne;
        newThread.ConnectorTwo = connectorTwo;

        if (newThread.ConnectorOne == null || newThread.ConnectorTwo == null)
        {
            ClassicalThread.Destroy(this);
            return;
        }
            
        newThread.ReplaceThread();
        newThread.ConnectorOne.AddThread(newThread);
        newThread.ConnectorTwo.AddThread(newThread);
        newThread.IsNot = isNot;
        newThread.noDrag = true;
    }

    public void RemoveThread(Connector connectorOne, Connector connectorTwo)
    {
        for (int i = 0; i < _threads.Count; i++)
        {
            if ((_threads[i].ConnectorOne == connectorOne && _threads[i].ConnectorTwo == connectorTwo)
                    || (_threads[i].ConnectorTwo == connectorOne && _threads[i].ConnectorOne == connectorTwo))
            {
                ClassicalThread.Destroy(_threads[i]);
                break;
            }
        }
    }

    public void ReplaceThreads()
    {
        foreach (ClassicalThread thread in _threads)
            thread.ReplaceThread();
    }

    public void NegateThread(Connector connectorOne, Connector connectorTwo)
    {
        for (int i = 0; i < _threads.Count; i++)
        {
            if ((_threads[i].ConnectorOne == connectorOne && _threads[i].ConnectorTwo == connectorTwo)
                    || (_threads[i].ConnectorTwo == connectorOne && _threads[i].ConnectorOne == connectorTwo))
            {
                _threads[i].IsNot ^= true;
                break;
            }
        }
    }

    public void SetGridHandler(GridHandler handler)
    {
        _gridHandler = handler;
    }
}
