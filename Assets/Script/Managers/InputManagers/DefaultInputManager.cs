using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;
using System.Linq;
using System;


public class DefaultInputManager : MonoBehaviour, IInputManager
{
    //Ideally, to remove at the end
    private LevelManager _levelManager;

    [SerializeField, Range(1, 5)] float _maxZoomValue = 2f;
    [SerializeField, Range(0.1f, 1)] float _zoomStep = 0.5f;
   	//[SerializeField] Transform _cursorTransform;

    private Vector2 _mousePosition;
    private Camera _mainCamera;
    private SpatialMode _selectedSpatialMode = null;
    private Transform _draggedObject;
    private IDraggable _draggableComponent;
    private bool _isDragging = false;
    private IList<GameObject> _selected = new List<GameObject>();
    private Transform _ticketParent;
    private TicketReceptorMulti[] _receptorsMulti;
    private GridHandler _gridHandler;
    private IDialogueManager _dialogueManager;
    private IDisplayerManager _displayManager;
    private IHistoryManager _historyManager;
    private float _originalZoom;
    private float _scaleStep;
    private UnityEngine.UI.Toggle _classicalToggle;

    public IDialogueManager DialogueManager
    {
        get 
        { 
            if (_dialogueManager == null)
                _levelManager.QueryDialogueManager(this);
            return (_dialogueManager);
        }
    }

    public void Init()
    {
        _mainCamera = Camera.main;
        _levelManager = LevelManager.Instance;
        _ticketParent = _levelManager.TicketParent;
        _originalZoom = _mainCamera.orthographicSize;
        _scaleStep = _ticketParent.transform.localScale.x / _mainCamera.orthographicSize * _zoomStep;
        _receptorsMulti = _ticketParent.GetComponentsInChildren<TicketReceptorMulti>();
        _levelManager.QueryGridHandler(this);
        _levelManager.QueryDialogueManager(this);
        _levelManager.QueryDisplayerManager(this);
        _levelManager.QueryHistoryManager(this);
        _classicalToggle = _levelManager.ClassicalToggle;
    }

    /// <summary>
    /// Appel� � chaque fois que la souris bouge sur l'�cran. Si un objet est en train d'�tre d�plac�, bouge cet objet en appelant la fonction DragObject.
    /// Les seuls objets pouvant �tre d�plac� sont les objets héritant de l'interface IDraggable.
    /// </summary>
    /// <param name="data"></param>
    public void OnPosition(InputValue data)
    {
        Vector2 position;

        position = data.Get<Vector2>();
        if (_mainCamera && _mainCamera.pixelRect.Contains(position))
        {
            _mousePosition = position;
			Vector3 worldPos = _mainCamera.ScreenToWorldPoint(_mousePosition);
			//_cursorTransform.position = worldPos + Vector3.forward * 100f;
            if (_isDragging)
            {
                
                _draggableComponent.DragObject(new Vector3(worldPos.x, worldPos.y, _draggedObject.position.z), _mousePosition);
            }
        }

    }

    /// <summary>
    /// This function is called whenever the player click somewhere. It will check wether the player clicked on something which should be 
    /// dragged (as the handle of a Slider or an Instrument) or a ticket which will spawn an Instrument.
    /// It is also called when the button is released, we use it as a signel to stop dragging the object and check wether the click was a dragging
    /// or an object selection.
    /// </summary>
    /// <param name="data"></param>
    public void OnClickPlayer(InputValue data)
    {
        Ray inputRay = _mainCamera.ScreenPointToRay(_mousePosition);
        RaycastHit2D hitResults = new RaycastHit2D();

        if (data.isPressed && _levelManager.IsLevelStateUI())
        {
            if (_dialogueManager == null)
                _levelManager.QueryDialogueManager(this);
            _dialogueManager.OnClick();
            return;
        }
        if (data.isPressed && _levelManager.IsLevelStateBook())
            return;
        UnselectSpatialMode();
        if (data.isPressed && _levelManager.IsLevelStatePaused())
        {
            RaycastHit2D spatialModeHit = Physics2D.Raycast(inputRay.origin, inputRay.direction, 1000f,
                                        LayerMask.GetMask("SpatialModes"));
            if (spatialModeHit.transform != null && spatialModeHit.transform.gameObject.TryGetComponent(out SpatialMode mode))
            {
                mode.SelectSpatialMode();
                _selectedSpatialMode = mode;
                _displayManager.DisplayFaketon(mode);
            }
        }
        else
        {
            _displayManager.RemoveFromScreen();
        }
        /*else if (data.isPressed)
        {
            _displayManager.RemoveFromScreen();
        }*/



        if (!_isDragging && data.isPressed) // We just clicked: are we going to drag something?
        {
            if (_levelManager.IsLevelStateCleared())
            {
                if (_levelManager.IsInClassicalMode == false)
                {
                    hitResults = Physics2D.Raycast(inputRay.origin, inputRay.direction, 1000f,
                                                                LayerMask.GetMask("GameUI", "Instruments"));
                }
                else
                {
                    hitResults = Physics2D.Raycast(inputRay.origin, inputRay.direction, 1000f,
                                                                LayerMask.GetMask("Classical", "Classical Thread"));
                }
            }
            if (!_levelManager.IsLevelStateWon() && hitResults.transform == null)
            {
                ClearSelection();
                hitResults = Physics2D.Raycast(inputRay.origin, inputRay.direction, 1000f,
                                               LayerMask.GetMask("GridBack"));
            }
            if (hitResults.transform != null) // Bingo we touched an object!
            {
                // Let's see what we touched:
                GameObject hitObject = hitResults.transform.gameObject;
                // We check if the hit object is a Ticket. Which mean it will spawn a draggable object and drag it.
                // Or if it Draggable. It is is it will call the BeginDrag method of the object
                if (hitObject.TryGetComponent(out ITicket ticketComponent))
                {
                    _draggedObject = ticketComponent.InstantiateDraggableObject(hitResults.transform.position);
                    if (_draggedObject != null)
                    {
                        _isDragging = true;
                        _draggableComponent = _draggedObject.GetComponent<IDraggable>();
                        _draggableComponent.BeginDrag(hitResults.transform.position, _mousePosition);
                    }
                }
                else if (hitObject.TryGetComponent(out _draggableComponent))
                {
                    _isDragging = true;
                    _draggedObject = hitResults.transform;
                    _draggableComponent.BeginDrag(hitResults.transform.position, _mousePosition);
                }
            }
            else
            {
                ClearSelection();
            }
        }
        else if (_isDragging && !data.isPressed) // We are releasing our drag
        {
            _isDragging = false;
            _draggableComponent.EndDrag(_mainCamera.ScreenToWorldPoint(_mousePosition), _mousePosition);
            _draggedObject = null;
            _draggableComponent = null;
        }
    }

    //Multple selection commented for now
    /// <summary>
    /// Same as OnClickPlayer but for multi-select
    /// </summary>
    /// <param name="data"></param>
    /*public void OnCtrlClickPlayer(InputValue data)
    {
        if (!_levelManager.IsLevelStateCleared())
            return;
        if (data.isPressed)
        {
            // If the object clicked on is selectable (now: only Instruments are), select it. Else, deselect the current selection.
            Ray inputRay = _mainCamera.ScreenPointToRay(_mousePosition);
            RaycastHit2D hitResults = Physics2D.Raycast(inputRay.origin, inputRay.direction, 100f, 1 << 3);
            if (hitResults.transform != null)
            {
                if (hitResults.transform.gameObject.TryGetComponent(out Instrument instrument))
                {
                    instrument.selector.SetActive(true);
                    //instrument.selector.GetComponent<SpriteRenderer>().enabled = true;
                    if (!_selected.Contains(instrument.gameObject))
                    {
                        _selected.Add(instrument.gameObject);
                    }
                }

            }
            else
            {
                ClearSelection();
            }
        }
    }*/



    /// <summary>
    /// Is called when the Delete (suppr for now) key is pressed. It removes every selected object which can be modified.
    /// </summary>
    /// <param name="data"></param>
    public void OnDelete(InputValue data)
    {
        List<GameObject> newSelected = new List<GameObject>();

        if (data.isPressed)
        {
            foreach (GameObject obj in _selected)
            {
                // This part should be refactored. one interface IDestroyable, we check if interface is here and call the Destroy function
                Instrument associatedInstrument = obj.GetComponent<Instrument>();
                if (associatedInstrument != null)
                {

                    if (associatedInstrument.isDraggable)
                    {
                        _historyManager.AddRemoveInstrumentAction(associatedInstrument);
                        _gridHandler.FreeCell(obj.transform.position);
                        Instrument.Destroy(associatedInstrument);
                    }
                    else
                        newSelected.Add(obj);
                }
                else
                {
                    var classicalThread = obj.GetComponent<ClassicalThread>();
                    _historyManager.AddRemoveThreadAction(classicalThread);
                    ClassicalThread.Destroy(classicalThread);
                }
            }
            _selected = newSelected;
        }
    }

    /// <summary>
    /// Is called when the Rotate (R for now) key is pressed. It rotates every selected object which can be modified.
    /// </summary>
    /// <param name="data"></param>
    public void OnRotate(InputValue data)
    {
        if (data.isPressed && !_levelManager.IsInClassicalMode)
        {
            foreach (GameObject obj in _selected)
            {
                Instrument instrument = obj.GetComponent<Instrument>();
                Orientation newOrientation = instrument.RotateClockwise();
                _gridHandler.ChangeDataOrientation(obj.transform.position, newOrientation);
                _historyManager.AddRotateInstrument(instrument);
            }
        }
    }

    public void OnRotateButton()
    {
        foreach (GameObject obj in _selected)
        {
            Instrument instrument = obj.GetComponent<Instrument>();
            Orientation newOrientation = instrument.RotateClockwise();
            _gridHandler.ChangeDataOrientation(obj.transform.position, newOrientation);
        }
    }

    public void OnNegate(InputValue data)
    {
        foreach (TicketReceptorMulti receptor in _receptorsMulti)
        {
            receptor.RedrawWithPhase();
        }
        /*if (data.isPressed && _levelManager.IsInClassicalMode)
        {
            foreach (GameObject obj in _selected)
            {
                var classicalThread = obj.GetComponent<ClassicalThread>();
                classicalThread.SwitchNotMode();
                _historyManager.AddNegateThreadAction(classicalThread);
            }
        }*/
    }

    public void OnClassical()
    {
        if (_classicalToggle.gameObject.activeInHierarchy)
            _classicalToggle.isOn ^= true;
    }

    public void OnSkip()
    {
        if (_levelManager.IsLevelStateUI())
            DialogueManager.Skip();
        else if (_levelManager.IsLevelStateBook())
            _levelManager.CloseBook();
    }


    public void OnZoom(InputValue data)
    {
        ClearSelection();
        float val = data.Get<float>();

        if (_selectedSpatialMode != null)
        {
            if (val > 0f)
                _displayManager.Scrolling(-1);
            else if (val < 0f)
                _displayManager.Scrolling(1);
        }
        else
        {
            if (val > 0.1f && _mainCamera.orthographicSize > _originalZoom / _maxZoomValue)
            {
                _mainCamera.orthographicSize -= _zoomStep;
                _gridHandler.UpdateZoom(_mainCamera.orthographicSize / _originalZoom);
                // J'ai honte de fou, c'est pour update les �ventuelle ligne d'intrications des fauxtons, vu qu'elles se redrawent que si on les activent
                // Update 04/09/2021 : j'ai toujours honte
                _ticketParent.gameObject.SetActive(false);
                _ticketParent.localScale = (Vector3.up + Vector3.right) * (_ticketParent.localScale.x - _scaleStep) + Vector3.forward * _ticketParent.localScale.z;
                _ticketParent.gameObject.SetActive(true);
            }
            else if (val < -0.1f && _mainCamera.orthographicSize < _originalZoom)
            {
                _mainCamera.orthographicSize += _zoomStep;
                _gridHandler.UpdateZoom(_mainCamera.orthographicSize / _originalZoom);
                _ticketParent.gameObject.SetActive(false);
                _ticketParent.localScale = (Vector3.up + Vector3.right) * (_ticketParent.localScale.x + _scaleStep) + Vector3.forward * _ticketParent.localScale.z;
                _ticketParent.gameObject.SetActive(true);
            }
        }
    }

    public void OnUndo()
    {
        if (_levelManager.IsLevelStateCleared())
            _historyManager.UndoLastAction();
    }

    public void ClearSelection()
    {
        foreach (GameObject obj in _selected)
        {
            if (obj != null)
            {
                obj.TryGetComponent(out Instrument inst);
                if (inst != null)
                    inst.selector.SetActive(false);
                else
                {
                    obj.GetComponent<ClassicalThread>().ChangeColorBack();
                }
            }

        }
        _selected.Clear();
    }

    public bool IsSelected(GameObject gameObject)
    {
        return (_selected.Contains(gameObject));
    }

    public void AddToSelection(GameObject gameObject)
    {
        _selected.Add(gameObject);
    }

    public void RemoveFromSelection(GameObject gameObject)
    {
        _selected.Remove(gameObject);
    }

    public void UnselectSpatialMode()
    {
        if (_selectedSpatialMode != null)
        {
            _selectedSpatialMode.UnSelectSpatialMode();
            _selectedSpatialMode = null;
        }
    }

    public void SetGridHandler(GridHandler gridHandler)
    {
        _gridHandler = gridHandler;
    }

    public void SetDialogueManager(IDialogueManager manager)
    {
        _dialogueManager = manager;
    }

    public void SetDisplayerManager(IDisplayerManager manager)
    {
        _displayManager = manager;
    }

    public void SetHistoryManager(IHistoryManager manager)
    {
        _historyManager = manager;
    }
}


