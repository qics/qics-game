using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FullDisplayer : MonoBehaviour, IDisplayerManager
{
    [SerializeField] List<StateDrawerUI> _listOfStateDrawer;
    [SerializeField] GameObject _background;
    [SerializeField] GameObject _paginationGameObject;

    private TMP_Text _pagination;
    private int _numberOfDisplayedState;
    private List<SpatialMode> _selectedSpatialMode;
    private int _rankToDisplay = 0;
    private int _indexOfMode = -1;
    private Faketon _currentFaketon = null;
    private float _dirtyOffset;

    public void Init()
    {
        _numberOfDisplayedState = _listOfStateDrawer.Count;
        RemoveFromScreen();
        if (_numberOfDisplayedState == 0)
        {
            Debug.LogWarning("No state displayer");
        }
        if (_paginationGameObject != null)
        {
            _dirtyOffset = _paginationGameObject.transform.localPosition.y;
        }
        _pagination = _paginationGameObject.GetComponentInChildren<TMP_Text>();
        _selectedSpatialMode = new List<SpatialMode>();
    }

    public void RemoveFromScreen()
    {
        _indexOfMode = -1;
        _currentFaketon = null;
        if (_background)
            _background.SetActive(false);
        if (_paginationGameObject)
            _paginationGameObject.SetActive(false);
        foreach (StateDrawerUI drawer in _listOfStateDrawer)
        {
            drawer.gameObject.SetActive(false);
        }
    }

    public void DisplayFaketon(SpatialMode mode)
    {
        int numberOfModeToDisplay;
        int numberOfStateToDisplay;

        _currentFaketon = mode.owner;
        _indexOfMode = _currentFaketon.spatialModes.IndexOf(mode);
        numberOfModeToDisplay = _currentFaketon.numberOfSpatialModes;
        _rankToDisplay = 0;
        if (_background != null)
        {
            _background.SetActive(true);
            if (_currentFaketon.stateList.Count < _numberOfDisplayedState)
                numberOfStateToDisplay = _currentFaketon.stateList.Count;
            else
                numberOfStateToDisplay = _numberOfDisplayedState;
            _background.transform.localScale = new Vector3(3.6f + 0.75f * numberOfModeToDisplay, numberOfStateToDisplay * 0.75f + 0.25f, _background.transform.localScale.z);

        }
        if (_paginationGameObject && numberOfModeToDisplay > 10)
            _paginationGameObject.SetActive(true);

        DisplayFromRank();
    }

    private void DisplayFromRank()
    {
        int i = 0;
        List<State> stateList = _currentFaketon.stateList;
        int numberOfStateToEffectivelyDisplay = Mathf.Min(_numberOfDisplayedState, stateList.Count);

        while (i < numberOfStateToEffectivelyDisplay)
        {
            _listOfStateDrawer[i].transform.localPosition = ((numberOfStateToEffectivelyDisplay - 1) / 2f - i) * 0.75f * Vector3.up + 20f * Vector3.forward;
            _listOfStateDrawer[i].gameObject.SetActive(true);
            _listOfStateDrawer[i].DrawState(stateList[i + _rankToDisplay], _indexOfMode);
            i = i + 1;
        }
        if (_paginationGameObject != null)
        {
            _paginationGameObject.transform.localPosition = ((-numberOfStateToEffectivelyDisplay - 1) / 2f * 0.75f + _dirtyOffset) * Vector3.up
                                    + 20f * Vector3.forward
                                    + _paginationGameObject.transform.localPosition.x * Vector3.right;
            if (_pagination != null)
            {
                _pagination.text = (_rankToDisplay + 1).ToString() + "-"
                                        + (_rankToDisplay + numberOfStateToEffectivelyDisplay).ToString()
                                        + "/" + (stateList.Count).ToString();
            }
        }


    }

    public void Scrolling(int direction)
    {
        int newRank;

        if (_currentFaketon == null)
            return;

        newRank = _rankToDisplay + direction;

        if (newRank >= 0 && newRank + _numberOfDisplayedState <= _currentFaketon.stateList.Count)
        {
            _rankToDisplay = newRank;
            DisplayFromRank();
        }

    }
}
