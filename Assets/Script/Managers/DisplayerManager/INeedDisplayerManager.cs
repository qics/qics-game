using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INeedDisplayManager
{
    public void SetDisplayerManager(IDisplayerManager manager);
}
