﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class GridData
{
    [SerializeField] Vector2IntSerializable _coordSerial;
    public Vector2Int Coord
    {
        get { return (Vector2IntSerializable.SerializeToV2(_coordSerial)); }
        set
        {
            _coordSerial = Vector2IntSerializable.V2ToSerialize(value);
        }
    }

    public int instrumentId;
    public Orientation instrumentOrientation;
    public bool isDraggable;
    public int[] specificInstruData;
    // This one is only for run time purpose, we don't need to serialize the whole class for the mean of saving/loading
    // However, during run time, it is confortable to know precisely which object is here
    [NonSerialized] public Instrument AssociatedInstrument;

    public GridData(Vector2Int coordinate, int id, Instrument instrument)
    {
        _coordSerial = Vector2IntSerializable.V2ToSerialize(coordinate);
        instrumentId = id;
        specificInstruData = instrument.GetSpecificData();
        instrumentOrientation = instrument.m_orientation;
        AssociatedInstrument = instrument;
        isDraggable = instrument.isDraggable;
    }

    public GridData(Vector2Int coordinate, int id, int[] specificInstrumentData, Orientation orientation)
    {
        _coordSerial = Vector2IntSerializable.V2ToSerialize(coordinate);
        instrumentId = id;
        specificInstruData = specificInstrumentData;
        instrumentOrientation = orientation;
        isDraggable = true;
    }

}

[Serializable]
public class Vector2IntSerializable
{
    public int x;
    public int y;

    public Vector2IntSerializable(int newX, int newY)
    {
        x = newX;
        y = newY;
    }

    public static Vector2Int SerializeToV2(Vector2IntSerializable coordSer)
    {
        return (new Vector2Int(coordSer.x, coordSer.y));
    }

    public static Vector2IntSerializable V2ToSerialize(Vector2Int coordInt)
    {
        return (new Vector2IntSerializable(coordInt.x, coordInt.y));
    }
    public string tempToString()
    {
        return ("(" + x + "," + y + ")");
    }
}

[Serializable]
public class Vector3Serializable
{
    public float x;
    public float y;
    public float z;

    public Vector3Serializable(float newX, float newY, float newZ)
    {
        x = newX;
        y = newY;
        z = newZ;
    }

    public static Vector3 SerializeToV3(Vector3Serializable coordSer)
    {
        return (new Vector3(coordSer.x, coordSer.y, coordSer.z));
    }

    public static Vector3Serializable V3ToSerialize(Vector3 coord)
    {
        return (new Vector3Serializable(coord.x, coord.y, coord.z));
    }
}

[Serializable]
public struct ThreadData
{
    [SerializeField] Vector2IntSerializable _instruIndexSerial;
    public Vector2Int InstruIndex
    {
        get { return (Vector2IntSerializable.SerializeToV2(_instruIndexSerial)); }
        set
        {
            _instruIndexSerial = Vector2IntSerializable.V2ToSerialize(value);
        }
    }
    /* [SerializeField] Vector3Serializable _connectorOneCoordSerial;
     public Vector3 ConnectorOneCoord
     {
         get { return (Vector3Serializable.SerializeToV3(_connectorOneCoordSerial)); }
         set
         {
             _connectorOneCoordSerial = Vector3Serializable.V3ToSerialize(value);
         }
     }

     [SerializeField] Vector3Serializable _connectorTwoCoordSerial;
     public Vector3 ConnectorTwoCoord
     {
         get { return (Vector3Serializable.SerializeToV3(_connectorTwoCoordSerial)); }
         set
         {
             _connectorTwoCoordSerial = Vector3Serializable.V3ToSerialize(value);
         }
     }*/

    public bool isNot;

    public ThreadData(Vector2Int newInstruIndex, bool newIsNot)
    {
        _instruIndexSerial = Vector2IntSerializable.V2ToSerialize(newInstruIndex);
        isNot = newIsNot;
    }
    /*public ThreadData(Vector3 coordinateOne, Vector3 coordinateTwo, bool newIsNot)
    {
        _connectorOneCoordSerial = Vector3Serializable.V3ToSerialize(coordinateOne);
        _connectorTwoCoordSerial = Vector3Serializable.V3ToSerialize(coordinateTwo);
        isNot = newIsNot;
    }*/
}

[Serializable]
public class LevelData
{
    public int[] perf;
    public List<GridData> gridDatas;
    public List<ThreadData> threadDatas;
    public bool isComplete = false;

    public LevelData()
    {
        //We keep track of 3 differents performance :
        //    0 : Number of cycle to succeed
        //    1 : Number of instruments to succeed
        //    2 : Number of thread to succeed
        perf = new int[3];
        perf[0] = -1;
        perf[1] = -1;
        perf[2] = -1;
    }
}

/// <summary>This class contains every data that needs to be saved,
/// the number of lives and the current progression
///  in maps.</summary>
[Serializable]
public class UserData
{
    public int currentLevel;
    public Dictionary<string, LevelData> levelDatas;

    public UserData()
    {
        currentLevel = 0;
        levelDatas = new Dictionary<string, LevelData>();
    }
}
