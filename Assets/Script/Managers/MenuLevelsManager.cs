﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuLevelsManager : MonoBehaviour
{
    private GameManager gameManager;
    [SerializeField] private LevelButton[] button_list;
    protected void Awake()
    {
        gameManager = GameManager.Instance;
        button_list = GetComponentsInChildren<LevelButton>();
        UserData userData = gameManager.userData;
        bool prerequesiteAreComplete;
        foreach (LevelButton btn in button_list)
        {
            //Debug.Log(btn.name);
            //Debug.Log(btn.AssociatedLevel);
            var levelNameList = gameManager.levelPrerequisites[btn.AssociatedLevel];
            prerequesiteAreComplete = true;
            if (!gameManager.DEBUG_cheatMode)
            {
                foreach (string level in levelNameList)
                {
                    LevelData lvlData;

                    if (userData.levelDatas.TryGetValue(level, out lvlData) && lvlData != null)
                    {
                        if (!lvlData.isComplete)
                            prerequesiteAreComplete = false;
                    }
                    else { prerequesiteAreComplete = false; }

                }
            }
            btn.SetInteractable(prerequesiteAreComplete);
            //DEBUG: All unlocked

        }
    }

    public void StartLevel(LevelButton button)
    {
        SceneManager.LoadScene(button.AssociatedLevel, LoadSceneMode.Single);
    }

    public void GoBackToMenu()
    {
        if (gameManager == null)
            gameManager = GameManager.Instance;
        gameManager.LoadMenuScene();
    }
}
