using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using System;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "Default Level Data Manager", menuName = "Managers/DataManager/Default")]
public class DefaultLevelDataManager : ScriptableObject, ILevelDataManager
{
    private LevelData _currentLevelData;
    private GridHandler _gridHandler;
    private IThreadManager _threadManager;

    public void Init()
    {
        var levelManager = LevelManager.Instance;

        levelManager.QueryThreadManager(this);
        levelManager.QueryGridHandler(this);
        _currentLevelData = new LevelData();
        
    }

    public LevelData GetLevelData()
    {
        _currentLevelData.gridDatas = _gridHandler.GridDatas;
        _currentLevelData.threadDatas = _threadManager.GetThreadData();
        return (_currentLevelData);
    }

    public void SetLevel(LevelData levelData)
    {
        _currentLevelData = levelData;
        _gridHandler.SetInstrumentsOnGrid(_currentLevelData.gridDatas);
        _threadManager.SetThreads(_currentLevelData.threadDatas);
    }

    public int[] SetWin(int numberOfCycle)
    {
        //We keep track of 3 differents performance :
        //    0 : Number of cycle to succeed
        //    1 : Number of instruments to succeed
        //    2 : Number of thread to succeed
        GetLevelData();
        _currentLevelData.perf[0] = numberOfCycle;
        _currentLevelData.perf[1] = _gridHandler.GetNonDevPlacedInstrument().Count;
        _currentLevelData.perf[2] = _currentLevelData.threadDatas.Count;
        _currentLevelData.isComplete = true;


        return (_currentLevelData.perf);
    }



    public void SetGridHandler(GridHandler gridHandler)
    {
        _gridHandler = gridHandler;
    }

    public void SetThreadManager(IThreadManager manager)
    {
        _threadManager = manager;
    }
}
