using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHistoryManager : INeedGridHandler, INeedThreadManager

{
    public void Init();
    public void UndoLastAction();

    public void AddMoveInstrumentAction(Vector2Int currentPosition, Vector2Int oldPosition);
    public void AddSpawnInstrumentAction(Vector2Int position);
    public void AddRemoveInstrumentAction(Instrument removedInstrument);
    public void AddConfigInstrument(Instrument configuredInstrument, int[] oldSpecificData);
    public void AddRotateInstrument(Instrument rotatedInstrument);

    public void AddAddThreadAction(ClassicalThread thread);
    public void AddRemoveThreadAction(ClassicalThread thread);

    public void AddNegateThreadAction(ClassicalThread thread);




}

