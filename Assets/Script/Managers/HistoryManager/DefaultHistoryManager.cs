﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Un delegate pour chaque action:
//  Move : une position sur la grille � une autre;
//  Spawn from ticket : L'instrument spawn pour le tuer
//  Remove instrument : L'id de l'intrument en question, sa position, sa config + tous les fils qui lui �tait reli�
//  Modify Instument property : L'instrument + l'ancienne config
//  Rotate : Unrotate
//  Add Thread : juste le thread en question pour le tuer
//  Remove Thread : les instrus auxquel il �tait connect�
//  Negate : UnNegate

internal enum PlayerAction
{
    MoveInstrument,
    SpawnInstrument,
    RemoveInstrument,
    ConfigInstrument,
    RotateInstrument,
    AddThread,
    RemoveThread,
    NegateThread
}

internal struct ActionAndDatas
{
    public PlayerAction action;
    public int[] datas;
}

internal delegate void UndoMethod(int[] data);

[CreateAssetMenu(fileName = "Default History Manager", menuName = "Managers/HistoryManager/Default")]
public class DefaultHistoryManager : ScriptableObject, IHistoryManager
{
    [SerializeField] int _maxHistorySize = 100;
    private LevelManager _levelManager;
    private GridHandler _gridHandler;
    private IThreadManager _threadManager;
    private Dictionary<PlayerAction, UndoMethod> _methodsDictionnary;
    private List<ActionAndDatas> _history;

    public IThreadManager ThreadManager
    {
        get
        {
            if (_threadManager == null)
                _levelManager.QueryThreadManager(this);
            return (_threadManager);
        }
    }

    public void Init()
    {
        _levelManager = LevelManager.Instance;

        if (_maxHistorySize <= 0)
        {
            Debug.LogError("Maximum size of historic inferior to 0");
        }
        _methodsDictionnary = new Dictionary<PlayerAction, UndoMethod>()
        {
            { PlayerAction.MoveInstrument, UndoMoveInstrumentAction },
            { PlayerAction.SpawnInstrument, UndoSpawnInstrumentAction },
            { PlayerAction.RemoveInstrument, UndoRemoveInstrumentAction },
            { PlayerAction.ConfigInstrument, UndoConfigInstrument},
            { PlayerAction.RotateInstrument, UndoRotateInstrument},
            { PlayerAction.AddThread, UndoAddThreadAction},
            { PlayerAction.RemoveThread, UndoRemoveThreadAction},
            { PlayerAction.NegateThread, UndoNegateThreadAction}
        };
        _history = new List<ActionAndDatas>();
        _levelManager.QueryGridHandler(this);
        _levelManager.QueryThreadManager(this);
    }


    public void UndoLastAction()
    {
        if (_history.Count == 0)
            return;
        ActionAndDatas lastAction = PopHistory();
        _methodsDictionnary[lastAction.action].Invoke(lastAction.datas);
    }

    public void AddMoveInstrumentAction(Vector2Int currentPosition, Vector2Int oldPosition)
    {
        int[] dataToStore = new int[4];
        ActionAndDatas actionAndDatas;

        dataToStore[0] = currentPosition.x;
        dataToStore[1] = currentPosition.y;
        dataToStore[2] = oldPosition.x;
        dataToStore[3] = oldPosition.y;

        actionAndDatas.action = PlayerAction.MoveInstrument;
        actionAndDatas.datas = dataToStore;
        PushOnHistory(actionAndDatas);
    }

    private void UndoMoveInstrumentAction(int[] data)
    {
        if (data == null || data.Length < 4)
        {
            Debug.LogError("Incorrect number of data");
            return;
        }
        _gridHandler.MoveInstrumentOnGrid(new Vector2Int(data[0], data[1]), new Vector2Int(data[2], data[3]));
        ThreadManager.ReplaceThreads();

    }

    public void AddSpawnInstrumentAction(Vector2Int position)
    {
        int[] dataToStore = new int[2];
        ActionAndDatas actionAndDatas;

        dataToStore[0] = position.x;
        dataToStore[1] = position.y;

        actionAndDatas.action = PlayerAction.SpawnInstrument;
        actionAndDatas.datas = dataToStore;
        PushOnHistory(actionAndDatas);
    }


    private void UndoSpawnInstrumentAction(int[] data)
    {
        if (data == null || data.Length < 2)
        {
            Debug.LogError("Incorrect number of data");
            return;
        }
        _gridHandler.RemoveInstrumentOnGrid(new Vector2Int(data[0], data[1]));

    }

    public void AddRemoveInstrumentAction(Instrument removedInstrument)
    {
        int[] dataToStore;
        Vector2Int instrumentCoord = _gridHandler.WorldToGridCoord(removedInstrument.transform.position);
        ActionAndDatas actionAndDatas;
        GridData gridData = _gridHandler.GetGridDataFromCoord(instrumentCoord);
        int specificDataLength;
        int numberOfThreads;
        List<Vector2Int> linkedInstrumentPosition = new List<Vector2Int>();
        List<bool> threadIsNot = new List<bool>();
        int i = 1;
        Connector instrumentConnector = removedInstrument.GetComponentInChildren<Connector>();

        if (instrumentConnector)
        {
            foreach (ClassicalThread thread in instrumentConnector.classicalThreads)
            {
                if (instrumentConnector == thread.ConnectorOne)
                    linkedInstrumentPosition.Add(_gridHandler.WorldToGridCoord(thread.ConnectorTwo.transform.position));
                else
                    linkedInstrumentPosition.Add(_gridHandler.WorldToGridCoord(thread.ConnectorOne.transform.position));
                threadIsNot.Add(thread.IsNot);
                i = i + 1;
            }
            numberOfThreads = instrumentConnector.classicalThreads.Count;
        }
        else
            numberOfThreads = 0;

        if (gridData.specificInstruData == null)
            specificDataLength = 0;
        else
            specificDataLength = gridData.specificInstruData.Length;

        dataToStore = new int[5 + specificDataLength + numberOfThreads * 3];

        dataToStore[0] = instrumentCoord.x;
        dataToStore[1] = instrumentCoord.y;
        dataToStore[2] = gridData.instrumentId;
        dataToStore[3] = (int)gridData.instrumentOrientation;
        dataToStore[4] = specificDataLength;
        for (int j = 0; j < specificDataLength; j++)
            dataToStore[5 + j] = gridData.specificInstruData[j];
        i = 0;
        foreach (Vector2Int pos in linkedInstrumentPosition)
        {
            dataToStore[5 + specificDataLength + i * 3] = pos.x;
            dataToStore[6 + specificDataLength + i * 3] = pos.y;
            if (threadIsNot[i] == true)
                dataToStore[7 + specificDataLength + i * 3] = 1;
            else
                dataToStore[7 + specificDataLength + i * 3] = 0;
            i += 1;
        }

        actionAndDatas.action = PlayerAction.RemoveInstrument;
        actionAndDatas.datas = dataToStore;
        PushOnHistory(actionAndDatas);
    }

    private void UndoRemoveInstrumentAction(int[] data)
    {
        int specificDataLenght = data[4];
        int[] specificInstrumentData = new int[specificDataLenght];
        int numberOfThread = (data.Length - 5 - specificDataLenght) / 3;
        Instrument instrument;
        

        for (int i = 0; i < specificDataLenght; i++)
        {
            specificInstrumentData[i] = data[5 + i];
        }
        GridData gridData = new GridData(new Vector2Int(data[0], data[1]), data[2], specificInstrumentData, (Orientation)data[3]);
        instrument = _gridHandler.SetInstrumentOnGrid(gridData);
        if (numberOfThread > 0)
        {
            Connector instrumentConnector = instrument.GetComponentInChildren<Connector>();
            for (int j = 0; j < numberOfThread; j++)
            {
                Vector2Int otherInstrumentCoord = new Vector2Int(data[5 + specificDataLenght + j * 3], data[6 + specificDataLenght + j * 3]);
                Connector otherConnector = _gridHandler.GetInstrumentFromCoord(otherInstrumentCoord).GetComponentInChildren<Connector>();
                bool isNot = (data[7 + specificDataLenght + j * 3] == 1);

                ThreadManager.PlaceThread(instrumentConnector, otherConnector, isNot);
            }
        }
    }

    public void AddConfigInstrument(Instrument configuredInstrument, int[] oldSpecificData)
    {
        int[] dataToStore;
        Vector2Int position;
        ActionAndDatas actionAndDatas;

        dataToStore = new int[2 + oldSpecificData.Length];
        position = _gridHandler.WorldToGridCoord(configuredInstrument.transform.position);
        dataToStore[0] = position.x;
        dataToStore[1] = position.y;
        for (int i = 0; i < oldSpecificData.Length; i++)
        {
            dataToStore[2 + i] = oldSpecificData[i];
        }
        actionAndDatas.action = PlayerAction.ConfigInstrument;
        actionAndDatas.datas = dataToStore;
        PushOnHistory(actionAndDatas);
    }

    public void UndoConfigInstrument(int[] data)
    {
        int[] _oldSpecificData = new int[data.Length - 2];
        Instrument instrumentToChange = _gridHandler.GetInstrumentFromCoord(new Vector2Int(data[0], data[1]));

        for (int i = 0; i < data.Length - 2; i++)
        {
            _oldSpecificData[i] = data[i + 2];
        }
        instrumentToChange.SetSpecificData(_oldSpecificData);
    }

    public void AddRotateInstrument(Instrument rotatedInstrument)
    {
        ActionAndDatas actionAndDatas;
        int[] dataToStore;
        Vector2Int position;

        dataToStore = new int[2];
        position = _gridHandler.WorldToGridCoord(rotatedInstrument.transform.position);
        dataToStore[0] = position.x;
        dataToStore[1] = position.y;
        actionAndDatas.action = PlayerAction.RotateInstrument;
        actionAndDatas.datas = dataToStore;
        PushOnHistory(actionAndDatas);
    }
    
    private void UndoRotateInstrument(int[] data)
    {
        Instrument instrument  = _gridHandler.GetInstrumentFromCoord(new Vector2Int(data[0], data[1]));

        Orientation newOrientation = instrument.RotateCounterClockwise();
        _gridHandler.ChangeDataOrientation(instrument.transform.position, newOrientation);
    }

    public void AddAddThreadAction(ClassicalThread thread)
    {
        ActionAndDatas actionAndDatas;
        int[] dataToStore = new int[4];
        Vector2Int connectorOnePosition = _gridHandler.WorldToGridCoord(thread.ConnectorOne.transform.position);
        Vector2Int connectorTwoPosition = _gridHandler.WorldToGridCoord(thread.ConnectorTwo.transform.position);

        dataToStore[0] = connectorOnePosition.x;
        dataToStore[1] = connectorOnePosition.y;
        dataToStore[2] = connectorTwoPosition.x;
        dataToStore[3] = connectorTwoPosition.y;
        actionAndDatas.action = PlayerAction.AddThread;
        actionAndDatas.datas = dataToStore;
        PushOnHistory(actionAndDatas);
    }

    private void UndoAddThreadAction(int[] data)
    {
        Connector connectorOne = _gridHandler.GetInstrumentFromCoord(new Vector2Int(data[0], data[1])).GetComponentInChildren<Connector>();
        Connector connectorTwo = _gridHandler.GetInstrumentFromCoord(new Vector2Int(data[2], data[3])).GetComponentInChildren<Connector>();

        ThreadManager.RemoveThread(connectorOne, connectorTwo);
    }

    public void AddRemoveThreadAction(ClassicalThread thread)
    {
        ActionAndDatas actionAndDatas;
        int[] dataToStore = new int[5];
        Vector2Int connectorOnePosition = _gridHandler.WorldToGridCoord(thread.ConnectorOne.transform.position);
        Vector2Int connectorTwoPosition = _gridHandler.WorldToGridCoord(thread.ConnectorTwo.transform.position);

        dataToStore[0] = connectorOnePosition.x;
        dataToStore[1] = connectorOnePosition.y;
        dataToStore[2] = connectorTwoPosition.x;
        dataToStore[3] = connectorTwoPosition.y;
        if (thread.IsNot == true)
            dataToStore[4] = 1;
        else
            dataToStore[4] = 0;
        actionAndDatas.action = PlayerAction.RemoveThread;
        actionAndDatas.datas = dataToStore;
        PushOnHistory(actionAndDatas);
    }

    private void UndoRemoveThreadAction(int[] data)
    {
        Connector connectorOne = _gridHandler.GetInstrumentFromCoord(new Vector2Int(data[0], data[1])).GetComponentInChildren<Connector>();
        Connector connectorTwo = _gridHandler.GetInstrumentFromCoord(new Vector2Int(data[2], data[3])).GetComponentInChildren<Connector>();
        bool isNot = (data[4] == 1);
        ThreadManager.PlaceThread(connectorOne, connectorTwo, isNot);
    }

    public void AddNegateThreadAction(ClassicalThread thread)
    {
        ActionAndDatas actionAndDatas;
        int[] dataToStore = new int[4];
        Vector2Int connectorOnePosition = _gridHandler.WorldToGridCoord(thread.ConnectorOne.transform.position);
        Vector2Int connectorTwoPosition = _gridHandler.WorldToGridCoord(thread.ConnectorTwo.transform.position);

        dataToStore[0] = connectorOnePosition.x;
        dataToStore[1] = connectorOnePosition.y;
        dataToStore[2] = connectorTwoPosition.x;
        dataToStore[3] = connectorTwoPosition.y;
        actionAndDatas.action = PlayerAction.NegateThread;
        actionAndDatas.datas = dataToStore;
        PushOnHistory(actionAndDatas);
    }

    private void UndoNegateThreadAction(int[] data)
    {
        Connector connectorOne = _gridHandler.GetInstrumentFromCoord(new Vector2Int(data[0], data[1])).GetComponentInChildren<Connector>();
        Connector connectorTwo = _gridHandler.GetInstrumentFromCoord(new Vector2Int(data[2], data[3])).GetComponentInChildren<Connector>();

        ThreadManager.NegateThread(connectorOne, connectorTwo);
    }


    private void PushOnHistory(ActionAndDatas toPush)
    {
        if (_history.Count == _maxHistorySize)
            _history.RemoveAt(0);
        _history.Add(toPush);
    }

    private ActionAndDatas PopHistory()
    {
        ActionAndDatas poped = _history[_history.Count - 1];
        _history.RemoveAt(_history.Count - 1);
        return (poped);
    }

    public void SetGridHandler(GridHandler gridHandler)
    {
        _gridHandler = gridHandler;
    }

    public void SetThreadManager(IThreadManager manager)
    {
        _threadManager = manager;
    }

}
