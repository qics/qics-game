using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITicket
{
    IDraggable spawnedObject
    {
        get;
        set;
    }

    public Transform InstantiateDraggableObject(Vector3 position);
}
