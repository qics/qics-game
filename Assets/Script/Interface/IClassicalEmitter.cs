using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public interface IClassicalEmitter
{
    public void SendSignal(bool signal);

}
