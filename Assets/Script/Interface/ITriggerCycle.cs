using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITriggerCycle
{
    public void TriggerCycle();
    public void SubscribeCycle();
    public void UnsubscribeCycle();

}
