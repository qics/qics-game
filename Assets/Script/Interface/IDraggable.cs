using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public interface IDraggable
{
    public bool noDrag {get; set; }

    public void DragObject(Vector3 position, Vector2 mousePosition);

    public void BeginDrag(Vector3 position, Vector2 mousePosition);

    public void EndDrag(Vector3 position, Vector2 mousePosition);
}
