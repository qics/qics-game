using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ErrorText : MonoBehaviour, ITriggerClear
{
	LevelManager _levelManager;
	Language _currentLanguage;
	[SerializeField] TMP_Text errorText;
	[SerializeField] GameObject _children;
	
	public void Start()
	{
		_levelManager = LevelManager.Instance;
		_currentLanguage = GameManager.Instance.CurrentLanguage;
		SubscribeClear();
	}
	
	public void DisplayErrorMessage(ErrorType error)
	{
		_children.SetActive(true);
		if (_currentLanguage == Language.French)
		{
			if (error == ErrorType.Collision)
				errorText.text = "Collision entre fauxtons";
			else if (error == ErrorType.WrongFaketon)
				errorText.text = "Mauvais paquet";
			else if (error == ErrorType.TooMuchModes)
				errorText.text = "Trop de mode dans le paquet";
			else if (error == ErrorType.TooMuchStates)
				errorText.text = "Trop d'�tat possible";
			else if (error == ErrorType.OutOfSimulation)
				errorText.text = "Le paquet sort de la table optique !";
			else if (error == ErrorType.SeveralOwner)
				errorText.text = "Paquet dans plusieurs collecteur";
			else if (error == ErrorType.TwoDifferentState)
				errorText.text = "Un collecteur ne peut accepter qu'un paquet de fauxtons � la fois";
			else if (error == ErrorType.TooMuchFaketon)
				errorText.text = "Il ne peut pas y avoir plus de 6 fauxtons dans un paquet !";
			else
				errorText.text = "Error";
		}
		else if (_currentLanguage == Language.English)
		{
			if (error == ErrorType.Collision)
				errorText.text = "Collision between faketons";
			else if (error == ErrorType.WrongFaketon)
				errorText.text = "Wrong packet";
			else if (error == ErrorType.TooMuchModes)
				errorText.text = "Too many packets";
			else if (error == ErrorType.TooMuchStates)
				errorText.text = "Too many possible states";
			else if (error == ErrorType.OutOfSimulation)
				errorText.text = "Packet is out of the optic table!";
			else if (error == ErrorType.SeveralOwner)
				errorText.text = "A packet is sent throught several different collectors";
			else if (error == ErrorType.TwoDifferentState)
				errorText.text = "A collector can only accept one packet of faketons at a time";
			else if (error == ErrorType.TooMuchFaketon)
				errorText.text = "There cannot be more than 6 faketon in a packet!";
			else
				errorText.text = "Error";
		}
		else
			errorText.text = "Error";

	}
	
    public void SubscribeClear()
    {
        _levelManager.ClearEvent += TriggerClear;
    }

    public void UnsubscribeClear()
    {
        _levelManager.ClearEvent -= TriggerClear;
    }

    public void TriggerClear()
    {
        _children.SetActive(false);
    }
	
	
}
