using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phylactere : MonoBehaviour
{
    [SerializeField] Transform _highAnchorBackground;
    [SerializeField] Transform _lowAnchorBackground;
    [SerializeField] Transform _highAnchorInfo;
    [SerializeField] Transform _lowAnchorInfo;
    [SerializeField] Transform _background;
    [SerializeField] Transform _infos;
    [SerializeField] ContactFilter2D _filter;

    public Transform HighAnchorBackground
    {
        get
        {
            if (_highAnchorBackground == null)
                _highAnchorBackground = transform.Find("HighAnchorBackground");
            return (_highAnchorBackground);
        }
    }

    public Transform LowAnchorBackground
    {
        get
        {
            if (_lowAnchorBackground == null)
                _lowAnchorBackground = transform.Find("LowAnchorBackground");
            return (_lowAnchorBackground);
        }
    }

    public Transform HighAnchorInfo
    {
        get
        {
            if (_highAnchorInfo == null)
                _highAnchorInfo = transform.Find("HighAnchorInfo");
            return (_highAnchorInfo);
        }
    }

    public Transform LowAnchorInfo
    {
        get
        {
            if (_lowAnchorInfo == null)
                _lowAnchorInfo = transform.Find("LowAnchorInfo");
            return (_lowAnchorInfo);
        }
    }

    public Transform Background
    {
        get
        {
            if (_background == null)
                _background = transform.Find("Background");
            return (_background);
        }
    }

    public Transform Infos
    {
        get
        {
            if (_infos == null)
                _infos = transform.Find("Infos");
            return (_infos);
        }
    }

    private Vector3 _originalScale;
    private float _originalCameraSize;
    private BoxCollider2D _colliderBackground;
    private SpriteRenderer _renderer;
    private Camera _mainCamera;

    public void Awake()
    {
        _colliderBackground = Background.GetComponent<BoxCollider2D>();
        _renderer = Background.GetComponent<SpriteRenderer>();

        // Sale
        _mainCamera = Camera.main;
        _originalCameraSize = 5f;
        _originalScale = transform.localScale;
    }

    public void OnEnable()
    {
        Collider2D[] colliding = new Collider2D[1];

        Vector3 scale = _originalScale * _mainCamera.orthographicSize / _originalCameraSize;
        transform.localScale = new Vector3(scale.x, scale.y, transform.localScale.z);
        if (_colliderBackground.OverlapCollider(_filter, colliding) > 0)
        {
            _renderer.flipY = true;
            Background.position = LowAnchorBackground.position;
            Infos.position = LowAnchorInfo.position;
        }
    }

    public void OnDisable()
    {
        _renderer.flipY = false;
        Background.position = HighAnchorBackground.position;
        Infos.position = HighAnchorInfo.position;
    }
}
