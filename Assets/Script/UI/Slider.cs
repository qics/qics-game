﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;

[Serializable]
public class ValueChanged : UnityEvent<int> { }

public class Slider : MonoBehaviour, IDraggable, INeedHistoryManager
{
    public int minValue = 1;
    public int maxValue = 5;
    public int stepValue = 1;
    public int value = 1;
    public GameObject handle;
    public GameObject slider;
    public TextMeshPro valueText;
    public ValueChanged OnValueChanged;
    public bool isBool = false;
    // Dirty as hell, should refactor the slider system, we have both weak link by using event and strong link cause I needed it for
    // the history
    public Instrument associatedInstrument;
    public bool noDrag { get; set; }

    private int _numberOfStep;
    private Vector2 _originPoint;
    private float[] _handlePossiblePositions;
    private float[] _switchPositions;
    private int[] _oldSpecificData;
    private Vector2 _currentPosition;
    [SerializeField] Transform _anchorLeft;
    [SerializeField] Transform _anchorRight;
    [SerializeField] bool _isInteractable = true;
    [SerializeField] Transform[] _dots;
    private IHistoryManager _historyManager;

    public bool IsInteractable { get { return (_isInteractable); } set { _isInteractable = value; } }

    public IHistoryManager HistoryManager
    {
        get 
        {
            if (_historyManager == null)
                LevelManager.Instance.QueryHistoryManager(this);
            return (_historyManager);
        }
    }

    //private bool DEBUG_alreadyInit = false;

   /* public void Awake()
    {
        if (DEBUG_alreadyInit == false)
            InitSlider();
    }*/

    public void InitSlider(Instrument linkedInstrument, int valueToInit = -2)
    {
        associatedInstrument = linkedInstrument;
        InitSlider(valueToInit);
    }

    public void InitSlider(int valueToInit = -2)
    {
        //DEBUG_alreadyInit = true;
        LevelManager.Instance.QueryHistoryManager(this);
        float scaleSlider = _anchorRight.localPosition.x - _anchorLeft.localPosition.x;

        if (handle == null || slider == null)
        {
            Debug.LogError("Missing handle or slider in slider " + name);
            Destroy(gameObject);
        }
        if (OnValueChanged == null)
        {
            Debug.LogWarning("Slider " + name + " linked to no object.");
        }

        _numberOfStep = (maxValue - minValue + 1) / stepValue;
        _handlePossiblePositions = new float[_numberOfStep];
        _switchPositions = new float[_numberOfStep - 1];
        _handlePossiblePositions[0] = _anchorLeft.localPosition.x;
        for (int i = 1; i < _numberOfStep; i++)
        {
            _handlePossiblePositions[i] = _handlePossiblePositions[0] + i * scaleSlider / (_numberOfStep - 1);
            if (i != _numberOfStep - 1)
            {
                _dots[i - 1].localPosition = new Vector3(_handlePossiblePositions[i], 0f, 0f);
                _dots[i - 1].gameObject.SetActive(true);
            }
                
            _switchPositions[i - 1] = _handlePossiblePositions[i] - 1f / 2f * scaleSlider / (_numberOfStep - 1);
        }

        if (valueToInit >= -1)
            ChangeValue(valueToInit);
        else
        {
            OnValueChanged?.Invoke(value);
            handle.transform.localPosition = _handlePossiblePositions[(value - minValue) / stepValue] * Vector3.right + slider.transform.localPosition.y * Vector3.up + Vector3.forward * 0f;
        }
    }

    public void BeginDrag(Vector3 position, Vector2 mousePosition)
    {
        //Dunno why the compiler does not se this being an int[] except if I do this
        int[] oldData = associatedInstrument.GetSpecificData();

        _oldSpecificData = new int[oldData.Length];
        for (int i = 0; i < oldData.Length; i++)
        {
            _oldSpecificData[i] = oldData[i];
        }
        //oldData.Clone;
    }

    public void EndDrag(Vector3 position, Vector2 mousePosition)
    {
        int[] newSpecificData = associatedInstrument.GetSpecificData();

        DragObject(position, mousePosition);
        for (int i = 0; i < newSpecificData.Length; i++)
        {
            if (newSpecificData[i] != _oldSpecificData[i])
            {
                HistoryManager.AddConfigInstrument(associatedInstrument, _oldSpecificData);
                break;
            }
        }
    }

    public void DragObject(Vector3 position, Vector2 mousePosition)
    {
        Vector3 localPos = transform.InverseTransformPoint(position);
        float xPos = localPos.x;

        if (_isInteractable)
        {
            for (int i = 0; i < _numberOfStep - 1; i++)
            {
                if (xPos < _switchPositions[i])
                {
                    //handle.transform.localPosition = new Vector3(_handlePossiblePositions[i], slider.transform.localPosition.y, 0f);
                    if (!isBool)
                        ChangeValue(i * stepValue + minValue);
                    else
                        ChangeValueBool(0);
                    return;
                }

            }
            //handle.transform.localPosition = new Vector3(_handlePossiblePositions[_numberOfStep - 1], slider.transform.localPosition.y, 0f);
            if (!isBool)
                ChangeValue(maxValue);
            else
                ChangeValueBool(1);
        }
    }

    public void ChangeValue(int newValue)
    {
        int newHandleLocation = (newValue - minValue) / stepValue;

        if (value != newValue)
        {
            value = newValue;
            if (valueText != null)
            {
                if (newValue == 0)
                {
                    valueText.text = "Constant";
                    handle.transform.localPosition = new Vector3(_handlePossiblePositions[0], slider.transform.localPosition.y, 0f);
                }

                else if (newValue == -1)
                {
                    handle.transform.localPosition = new Vector3(_handlePossiblePositions[0], slider.transform.localPosition.y, 0f);
                    valueText.text = "Asservi";
                }
                else
                {
                    handle.transform.localPosition = new Vector3(_handlePossiblePositions[newHandleLocation], slider.transform.localPosition.y, 0f);
                    valueText.text = value.ToString();
                }

            }

            OnValueChanged?.Invoke(value);
        }
    }
    public void ChangeValueBool(int newValue)
    {
        if (value != newValue)
        {
            value = newValue;
            if (valueText != null && value == 0)
            {
                handle.transform.localPosition = new Vector3(_handlePossiblePositions[0], slider.transform.localPosition.y, 0f);
                valueText.text = "X";
            }
            else if (valueText != null)
            {
                valueText.text = "O";
                handle.transform.localPosition = new Vector3(_handlePossiblePositions[1], slider.transform.localPosition.y, 0f);
            }


            OnValueChanged?.Invoke(value);
        }
    }

    public void SetHistoryManager(IHistoryManager manager)
    {
        _historyManager = manager;
    }
}
