//using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Change thread mask dispacement to be related to cycles
//Add in emitter instrument the possibility to know wich action will be made next round based on who came first
public class ClassicalThread : MonoBehaviour, IDraggable, INeedThreadManager, INeedInputManager, INeedHistoryManager
{
    private SpriteRenderer _spriteRenderer;
    private LevelManager _levelManager;
    private IInputManager _inputManager;
    private IThreadManager _threadManager;
    private IHistoryManager _historyManager;
    private Vector2 _initialAnchor;
    private Color _currentColor = Color.red;
    // For now I put the prefab directly here but refactoring needs to be taking into account and put those two way of sharing classical as services
    [SerializeField] private GameObject _classicalSignalObject;


    public bool noDrag { get; set; }

    public Connector ConnectorOne { get; set; } = null;
    public Connector ConnectorTwo { get; set; } = null;
    //public Connector connectorEmitter = null;
    //public Connector connectorReceptor = null;
    private bool _isNot = false;
    public bool IsNot
    {
        get { return (_isNot); }
        set
        {
            if (value)
            {
                _isNot = true;
                _currentColor = Color.green;
            }
            else
            {
                _isNot = false;
                _currentColor = Color.red;
            }
            ChangeColorBack();
        }
    }

    public void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _levelManager = LevelManager.Instance;
        _levelManager.QueryInputManager(this);
        _levelManager.QueryThreadManager(this);
        _levelManager.QueryHistoryManager(this);
        SubscribeClassicalView();
        noDrag = false;
    }

    public void SetThreadManager(IThreadManager manager)
    {
        _threadManager = manager;
    }

    public void SetInputManager(IInputManager manager)
    {
        _inputManager = manager;
    }

    //To put into the accesoor
    public void SetConnectorOne(Connector connector)
    {
        ConnectorOne = connector;
    }

    public void SetConnectorTwo(Connector connector)
    {
        ConnectorTwo = connector;
    }

    public void TransportClassicalInformation(bool signalToPass, Connector emitter)
    {
        bool trueSignal = signalToPass ^ IsNot;
        Connector receptor;
        GameObject signalObject;
        int direction = 1;

        ReplaceThread();
        if (emitter == ConnectorOne)
        {
            receptor = ConnectorTwo;

        }
        else
        {
            receptor = ConnectorOne;
            direction = -1;
        }

        if (receptor.Type == ConnectorType.Emitter)
            return;

        signalObject = Instantiate(_classicalSignalObject, emitter.transform.position, transform.rotation, transform);
        signalObject.GetComponent<ClassicalSignal>().InitSignal((receptor.transform.position - emitter.transform.position).magnitude, direction, receptor, trueSignal);

        //gameObject.SetActive(true);
        //StartCoroutine(FadeOutThread(signalToPass));


    }

    private IEnumerator FadeOutThread(bool signalToPass)
    {
        //TEMP
        if (ConnectorOne.Type == ConnectorType.Receptor)
            ConnectorOne.ReceiveSignal(signalToPass);
        else
            ConnectorTwo.ReceiveSignal(signalToPass);
        _spriteRenderer.maskInteraction = SpriteMaskInteraction.None;
        while (_spriteRenderer.color.a >= 0)
        {
            _spriteRenderer.color = new Color(_spriteRenderer.color.r, _spriteRenderer.color.g, _spriteRenderer.color.b, _spriteRenderer.color.a - Time.deltaTime);

            yield return new WaitForEndOfFrame();
        }
        _spriteRenderer.color = _currentColor;
        gameObject.SetActive(false);

    }

    public static void Destroy(ClassicalThread thread)
    {
        if (thread.ConnectorOne)
            thread.ConnectorOne.RemoveThread(thread);
        if (thread.ConnectorTwo)
            thread.ConnectorTwo.RemoveThread(thread);
        thread.UnsubscribeClassicalView();
        thread._threadManager.RemoveThread(thread);
        Destroy(thread.gameObject);
    }

    public void ChangeColorBack()
    {
        _spriteRenderer.color = _currentColor;
    }

    public void SwitchNotMode()
    {
        if (IsNot)
            IsNot = false;
        else
            IsNot = true;
    }


    #region Drag Functions

    public void BeginDrag(Vector3 position, Vector2 mousePosition)
    {
        if (!noDrag)
        {
            Vector2 distance;

            transform.position = _levelManager.gridHandler.NearestGridCenter(position);
            transform.localPosition = transform.localPosition + Vector3.forward;
            _initialAnchor = (Vector2)transform.position;
            distance = (Vector2)position - _initialAnchor;
            _spriteRenderer.size = new Vector2(distance.magnitude, _spriteRenderer.size.y);
            transform.Rotate(-Vector3.forward, Vector2.SignedAngle(transform.right, distance));
        }
    }

    public void DragObject(Vector3 position, Vector2 mousePosition)
    {
        if (!noDrag)
        {
            Vector2 distance = (Vector2)position - _initialAnchor;

            _spriteRenderer.size = new Vector2(distance.magnitude, _spriteRenderer.size.y);
            transform.Rotate(Vector3.forward, Vector2.SignedAngle(transform.right, distance));
        }
    }

    public void EndDrag(Vector3 position, Vector2 mousePosition)
    {
        if (!noDrag)
        {
            Ray inputRay = new Ray(position, position + Vector3.forward);
            RaycastHit2D hitResults = Physics2D.Raycast(inputRay.origin, inputRay.direction, 1000f,
                                                        LayerMask.GetMask("Classical"));
            if (hitResults.transform == null)
            {
                ClassicalThread.Destroy(this);
            }
            else
            {
                //Debug.Log(hitResults.transform.name);
                Vector2 distance = (Vector2)hitResults.transform.position - _initialAnchor;
                Connector endConnector = hitResults.transform.GetComponent<Connector>();
                if (Connector.IsConnectedOrSameType(ConnectorOne, endConnector))
                {
                    //Debug.Log(endConnector.isEmiter + " " + Connector.CheckIfConnected(connectorEmitter, endConnector));
                    ClassicalThread.Destroy(this);
                }
                else
                {
                    endConnector.AddThread(this);
                    ConnectorOne.AddThread(this);
                    ConnectorTwo = endConnector;
                    _spriteRenderer.size = new Vector2(distance.magnitude, _spriteRenderer.size.y);
                    transform.Rotate(Vector3.forward, Vector2.SignedAngle(transform.right, distance));
                    noDrag = true;
                    _historyManager.AddAddThreadAction(this);
                }
            }
        }
        else
        {
            if (!_inputManager.IsSelected(gameObject))
            {
                _inputManager.ClearSelection();
                _spriteRenderer.color = Color.blue;
                _inputManager.AddToSelection(gameObject);
            }
            else
                _inputManager.ClearSelection();
        }


    }

    #endregion

    #region Classical View Event

    public void SubscribeClassicalView()
    {
        _levelManager.ClassicalViewEvent += ReplaceThread;
    }

    public void UnsubscribeClassicalView()
    {
        _levelManager.ClassicalViewEvent -= ReplaceThread;
    }

    public void ReplaceThread()
    {
        if (ConnectorOne != null && ConnectorTwo != null)
        {
            Vector2 distance;

            transform.position = ConnectorOne.transform.position;
            transform.localPosition = transform.localPosition + Vector3.forward;
            _initialAnchor = (Vector2)transform.position;
            distance = (Vector2)ConnectorTwo.transform.position - _initialAnchor;
            _spriteRenderer.size = new Vector2(distance.magnitude, _spriteRenderer.size.y);
            transform.Rotate(Vector3.forward, Vector2.SignedAngle(transform.right, distance));
        }
    }

    public Connector GetConnectorFromPosition(Vector3 position)
    {
        Ray inputRay = new Ray(position, position + Vector3.forward);
        RaycastHit2D hitResults = Physics2D.Raycast(inputRay.origin, inputRay.direction, 1000f,
                                                    LayerMask.GetMask("Classical"));

        if (hitResults.transform == null)
        {
            return (null);
        }
        return (hitResults.transform.GetComponent<Connector>());
    }


    #endregion

    public void SetHistoryManager(IHistoryManager manager)
    {
        _historyManager = manager;
    }
}
