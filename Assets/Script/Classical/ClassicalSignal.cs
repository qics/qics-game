using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ClassicalSignal : MonoBehaviour, ITriggerClear, ITriggerClassicalCycle
{
    private LevelManager _levelManager;
    private float _distance;
    private Vector3 _direction;
    private bool _signal;
    private Connector _connectorReceptor;
    private Vector3 _nextLocation;
    private float _precision = 0.01f;
    private bool _shouldSendSignal = false;
    private SpriteRenderer _renderer;

    public void Awake()
    {
        _levelManager = LevelManager.Instance;
        SubscribeClassicalCycle();
        SubscribeClear();
        _nextLocation = transform.localPosition;
        _renderer = GetComponent<SpriteRenderer>();
    }

    public void InitSignal(float distance, int directionSign, Connector receptor, bool signal)
    {
        _distance = distance;
        _connectorReceptor = receptor;
        _signal = signal;
        _direction = _levelManager.levelGrid.cellSize.x * Vector3.right * (float)directionSign;
        if (_signal == false)
            _renderer.color = Color.blue;
        TriggerClassicalCycle();
    }

    #region Cycle Methods

    public void TriggerClassicalCycle()
    {
        Vector3 velocity;

        StopAllCoroutines();
        transform.localPosition = _nextLocation;
        if (_shouldSendSignal)
            SendSignalAndDie();

        _distance -= _direction.magnitude;

        velocity = _direction / _levelManager.TimeBetweenCycles;
        if (_distance < 0.01f)
        {
            _shouldSendSignal = true;
            if (_distance < 0f)
                _nextLocation = transform.localPosition + _direction * (1f + _distance / _direction.magnitude);
            else
                _nextLocation = transform.localPosition + _direction;
        }
        else
            _nextLocation = transform.localPosition + _direction;
        StartCoroutine(Move(velocity, _nextLocation));
    }

    private void SendSignalAndDie()
    {
        _connectorReceptor.ReceiveSignal(_signal);
        DestroySignal();
    }

    private IEnumerator Move(Vector3 velocity, Vector3 nextLocation)
    {
        float currentDistance;
        float expectedDistance;
        Vector3 currentVec;
        Vector3 expectedVec;

        currentDistance = (nextLocation - transform.localPosition).magnitude;
        expectedDistance = (nextLocation - transform.localPosition - velocity * Time.fixedDeltaTime).magnitude;
        while (currentDistance >= _precision && expectedDistance < currentDistance)
        {
            currentDistance = (nextLocation - transform.localPosition).magnitude;
            expectedDistance = (nextLocation - transform.localPosition - velocity * Time.fixedDeltaTime).magnitude;
            currentVec = nextLocation - transform.localPosition;
            expectedVec = nextLocation - transform.localPosition - velocity * Time.fixedDeltaTime;
            if (currentVec.x * expectedVec.x < 0 || currentVec.y * expectedVec.y < 0)
                break;
            else
                transform.localPosition += velocity * Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
        transform.localPosition = nextLocation;
    }

    public void SubscribeClassicalCycle()
    {
        _levelManager.StartClassicalCycle += TriggerClassicalCycle;
    }

    public void UnsubscribeClassicalCycle()
    {
        _levelManager.StartClassicalCycle -= TriggerClassicalCycle;
    }

    #endregion

    #region Clear Methods
    public void SubscribeClear()
    {
        _levelManager.ClearEvent += TriggerClear;
    }

    public void UnsubscribeClear()
    {
        _levelManager.ClearEvent -= TriggerClear;
    }

    public void TriggerClear()
    {
        DestroySignal();
    }

    #endregion

    void DestroySignal()
    {
        UnsubscribeClassicalCycle();
        UnsubscribeClear();
        Destroy(gameObject);
    }
}
